package com.digicash.retailer.Recharge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class CustomerInfoActivity extends AppCompatActivity {
    ImageView img_profile_pic;
    // Typeface font ;
    TextView txt_title,txt_label1,txt_label2,txt_label3,txt_label4, txt_label5,txt_label6,txt_label7,txt_label8;
    String token;
    SharedPreferences sharedpreferences;
    RelativeLayout rel_no_records,rel_no_internet;
    TextView txt_err_msgg,txt_network_msg;
    LinearLayout progress_linear,linear_container;
    ImageView img_edit;
    Context ctx;
    String operator_id="", cust_number="";
    Button backButton;

    public CustomerInfoActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_customer_info);

        //img_profile_pic= (ImageView) getActivity().findViewById(R.id.img_profile_pic);
        txt_label1 = (TextView) findViewById(R.id.txt_label1);
        txt_label2 = (TextView) findViewById(R.id.txt_label2);
        txt_label3 = (TextView) findViewById(R.id.txt_label3);
        txt_label4 = (TextView) findViewById(R.id.txt_label4);
        txt_label5 = (TextView) findViewById(R.id.txt_label5);
        txt_label6 = (TextView) findViewById(R.id.txt_label6);
        txt_label7 = (TextView) findViewById(R.id.txt_label7);
        txt_label8 = (TextView) findViewById(R.id.txt_label8);

        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) findViewById(R.id.txt_network_msg);
        progress_linear= (LinearLayout) findViewById(R.id.loding);
        linear_container= (LinearLayout) findViewById(R.id.container);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        rel_no_internet= (RelativeLayout) findViewById(R.id.rel_no_internet);
        backButton= (Button) findViewById(R.id.backButton);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        rel_no_internet.setVisibility(View.GONE);

        operator_id=getIntent().getStringExtra("operator_id");
        cust_number=getIntent().getStringExtra("cust_number");
        Log.d("bundle123456",operator_id+" "+cust_number+" ");

        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
               /* Fragment frg = new DTHRechargeFragment();
                CustomerInfoActivity.this.getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg, "Beneficiary Lists")
                        .addToBackStack(null)
                        .commit();*/
            }
        });

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.GONE);
            new FetchCustomerDetails().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            rel_no_internet.setVisibility(View.VISIBLE);
        }

    }

    private class FetchCustomerDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                parameters.put("vcNumber",cust_number);
                this.response = new JSONObject(service.POST(DefineData.VIEW_DTH_CUSTOMER_INFO,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("res789654",operator_id+ " "+cust_number+" "+response+ "");
            if(response!=null) {
                try {
                    /*if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_err_msgg.setText(msg+"");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        rel_img_bg.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);
                    } else {*/
                    JSONArray jsonArray = response.getJSONArray("records");
                    if(jsonArray.length()!=0) {
                       // Log.d("forloope1","111");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            //Log.d("forloope2","222");
                            Item superHero = null;
                            JSONObject json2 = null;
                            try {
                                //Log.d("inside respose","inside respose");
                                json2 = jsonArray.getJSONObject(i);
                                String MonthlyRecharge = json2.getString("MonthlyRecharge");
                                String Balance = json2.getString("Balance");
                                String customerName = json2.getString("customerName");
                                String status = json2.getString("status");
                                String NextRechargeDate = json2.getString("NextRechargeDate");
                                /*String lastrechargedate = json2.getString("lastrechargedate");
                                String lastrechargeamount = json2.getString("lastrechargeamount");*/
                                String planname = json2.getString("planname");
                                txt_label1.setText(MonthlyRecharge+"");
                                txt_label2.setText(Balance+"");
                                txt_label3.setText(customerName+ "");
                                txt_label4.setText(status+"");
                                txt_label5.setText(NextRechargeDate+"");
                               /* txt_label6.setText(lastrechargedate+"");
                                txt_label7.setText(lastrechargeamount+ "");*/
                                txt_label8.setText(planname+"");

                                linear_container.setVisibility(View.VISIBLE);
                                rel_no_records.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txt_err_msgg.setText("No Records Found!");
                                linear_container.setVisibility(View.GONE);
                                rel_no_records.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);
                            }
                        }
                    }else{
                        txt_err_msgg.setText("No Records Found!");
                        linear_container.setVisibility(View.GONE);
                        rel_no_records.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                    // }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setText("No Records Found!");
                    linear_container.setVisibility(View.GONE);
                    rel_no_records.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.GONE);
                rel_no_records.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}

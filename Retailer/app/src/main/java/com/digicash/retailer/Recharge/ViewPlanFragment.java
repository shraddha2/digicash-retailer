package com.digicash.retailer.Recharge;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.HomePage.HomeFragment;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ViewPlanFragment extends android.app.Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edit_fname,edt_lname,edit_email,edit_phone,edit_business_nm,edit_address,edit_pin_code,edit_aadhar_no,edit_pan_no,edt_state,edt_business_circle, edt_city;
    TextView edit_mtScheme,edit_prepaidScheme,edit_postpaidScheme,txt_err_msgg;
    String fname,lname,email,phone,business_nm,address,pin_code,aadhar_no,pan_no,state_select,circle_select,select_city,mt_Scheme,prepaid_Scheme,postpaid_Scheme;
    Spinner mtScheme,prepaidScheme,postpaidScheme;
    private ArrayList<Item> mList = new ArrayList<>();
    String scheme_type="--Select--",mt_Scheme_id="0",prepaid_Scheme_id="0", postpaid_Scheme_id="0";
    Button btn_submit;
    String token="";
    List<Item> list_comp_type;
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_network_msg,txt_label2,txt_error;
    LinearLayout progress_linear,linear_container,city;
    RelativeLayout rel_no_records,rel_no_internet;
    String operator_circle_id, operator_circle_name, bank_id, bank_name,telecom_circle_id,telecom_circle_name;
    String SpinnerMt, SpinnerPostpaid, SpinnerPrepaid;
    String operator_id;

    public ViewPlanFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.activity_view_plan, container, false);
        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        Bundle bundle = getArguments();
        operator_id = bundle.getString("operator_id");
        Log.d("1stLog",operator_id);

        SpinnerAdapter adapter = new com.digicash.retailer.Recharge.SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, mList);

        list_comp_type=new ArrayList<>();
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        btn_submit= (Button) rootView.findViewById(R.id.btn_submit);



        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_err_msgg= (TextView) rootView.findViewById(R.id.txt_err_msgg);
        txt_network_msg= (TextView) rootView.findViewById(R.id.txt_network_msg);

        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        //rel_no_internet= (RelativeLayout) rootView.findViewById(R.id.rel_no_internet);
        rel_no_records= (RelativeLayout) rootView.findViewById(R.id.rel_no_records);


        linear_container.setVisibility(View.GONE);
        rel_no_records.setVisibility(View.GONE);
        //rel_img_bg.setVisibility(View.VISIBLE);
        progress_linear.setVisibility(View.GONE);
        //rel_no_internet.setVisibility(View.GONE);
        //txt_error.setVisibility(View.GONE);



        mtScheme= (Spinner) rootView.findViewById(R.id.spin_offer_type);

        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.VISIBLE);
            progress_linear.setVisibility(View.GONE);
            //rel_no_internet.setVisibility(View.GONE);
            new loadSpinnerData().execute();
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.GONE);
            //rel_no_internet.setVisibility(View.VISIBLE);
        }

        mtScheme.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                scheme_type = ((TextView)view.findViewById(R.id.txt_operator_name)).getText().toString();
                mt_Scheme_id =((TextView)view.findViewById(R.id.txt_id)).getText().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        return rootView;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private class loadSpinnerData extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            mList.clear();
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                this.response = new JSONObject(service.POST(DefineData.VIEW_BROWSE_PLAN,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("2ndLog",operator_id);
            Log.d("outur", response +DefineData.VIEW_BROWSE_PLAN);

            if(response!=null) {
                try {
                    /*if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(msg+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {*/
                    JSONObject resjson = response.getJSONObject("records");
                    JSONArray jsonArray=resjson.getJSONArray("FULLTT");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    json2 = jsonArray.getJSONObject(i);
                                    String rs = json2.getString("rs");
                                    String desc = json2.getString("desc");
                                    String validity = json2.getString("validity");
                                    String last_update = json2.getString("last_update");
                                    superHero = new Item(validity,rs, desc, last_update);
                                    Log.d("Array1",rs+" "+ desc+" "+validity+" "+last_update+" ");
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_error.setVisibility(View.VISIBLE);
                                    txt_error.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    progress_linear.setVisibility(View.GONE);
                                }
                                mList.add(superHero);
                            }
                            SpinnerAdapter adapter1 = new com.digicash.retailer.Recharge.SpinnerAdapter(getActivity(),R.layout.activity_state_list_adapter, mList);
                            mtScheme.setAdapter(adapter1);
                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                        }


                   // }
                }catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onResume() {
        super.onResume();

        DijiCashRetailer.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 3:
                if (null!=data) {
                    operator_circle_id = data.getStringExtra("operator_circle_id");
                    operator_circle_name = data.getStringExtra("operator_circle_name");
                    edt_state.setText(operator_circle_name + "");
                    city.setVisibility(View.VISIBLE);
                }
                break;
            case 6:
                if (null!=data) {
                    bank_id = data.getStringExtra("operator_circle_id");
                    bank_name = data.getStringExtra("operator_circle_name");
                    edt_city.setText(bank_name + "");

                }
                break;
            case 4:
                if (null!=data) {
                    telecom_circle_id = data.getStringExtra("operator_circle_id");
                    telecom_circle_name = data.getStringExtra("operator_circle_name");
                    edt_business_circle.setText(telecom_circle_name + "");
                }
                break;
        }
    }
}

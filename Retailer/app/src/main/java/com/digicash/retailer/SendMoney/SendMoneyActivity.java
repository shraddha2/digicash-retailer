package com.digicash.retailer.SendMoney;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.Model.DividerItemDecoration;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;
import com.digicash.retailer.Recharge.SuccessActivity;
import com.digicash.retailer.TransactionHistory.RechargeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class SendMoneyActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    SharedPreferences sharedpreferences;
    String token,beneficiaryId,transfer_amount;
    EditText edt_amt;
    Button btn_submit,btn_home;
    RadioGroup rg_trans_type;
    RadioButton rb_neft,rb_imps;
    String trans_type="", beneficairy_name="",acctNo="",bankName="",neftLimitRs="";
    TextView txt_error,neftMessage,txt_msg;
    ConstraintLayout progress_linear,linear_container,contentTransactionDetails,SendMoney,SuccessPage;
    String surcharge ,amount, netAmount ,processingFee , beneficiaryName , bankName1,total_surcharge, accountNo, totalAmount;
    String cust_id="", sender_no="", senderName="";
    private String sender_id;
    private String otp_up;
    String errorMsg,mobile_number="",errMsg;
    RecyclerView recyclerTransactionDetails,recyclerSuccessTransactionList;
    private List<Item> transactionDetailsList = new ArrayList<>();
    private MtSuccessAdapter transactionDetailsAdapter;

    private List<Item> successTransactionList = new ArrayList<>();
    private MtSuccessAdapter successTransactionAdapter;

    private List<Item> TransactionList = new ArrayList<>();
   /* private List<Item> TransactionList = new ArrayList<>();
    private RecyclerView recyclerView;
    private MtSuccessAdapter mAdapter;*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_money);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        String mess = getResources().getString(R.string.app_name);
        setTitle("Transfer Money");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        edt_amt=(EditText) findViewById(R.id.edt_amt);
        btn_submit=(Button) findViewById(R.id.btn_submit);
        rg_trans_type=(RadioGroup) findViewById(R.id.radioGroup);
        rb_neft=(RadioButton) findViewById(R.id.rb_neft);
        rb_imps=(RadioButton) findViewById(R.id.rb_imps);

        cust_id=getIntent().getStringExtra("cust_id");
        sender_no=getIntent().getStringExtra("sender_no");
        senderName=getIntent().getStringExtra("senderName");
        sender_id= sender_no;
        beneficiaryId=getIntent().getStringExtra("beneficiaryId");
        beneficairy_name=getIntent().getStringExtra("cust_name");
        acctNo=getIntent().getStringExtra("accountNo");
        bankName=getIntent().getStringExtra("bank_name");
        neftLimitRs=getIntent().getStringExtra("neftLimitRs");

        txt_error= (TextView) findViewById(R.id.txt_error);
        progress_linear= (ConstraintLayout) findViewById(R.id.loading);
        linear_container= (ConstraintLayout) findViewById(R.id.container);
        SendMoney = (ConstraintLayout) findViewById(R.id.SendMoney);
        SuccessPage = (ConstraintLayout) findViewById(R.id.SuccessPage);

        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);

        contentTransactionDetails = (ConstraintLayout) findViewById(R.id.contentTransactionDetails);
        recyclerTransactionDetails = (RecyclerView) findViewById(R.id.recyclerTransactionDetails);
        transactionDetailsAdapter = new MtSuccessAdapter(transactionDetailsList,SendMoneyActivity.this,"Mt Transaction");
        //recyclerTransactionDetails.addItemDecoration(new DividerItemDecoration(SendMoneyActivity.this));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SendMoneyActivity.this);
        recyclerTransactionDetails.setLayoutManager(mLayoutManager);
        recyclerTransactionDetails.setItemAnimator(new DefaultItemAnimator());
        recyclerTransactionDetails.setAdapter(transactionDetailsAdapter);
        //neftMessage = (TextView) findViewById(R.id.neftMessage);
        //int  SelectedButtonValueId = type.getCheckedRadioButtonId();

        recyclerSuccessTransactionList = (RecyclerView) findViewById(R.id.recyclerSuccessTransactionList);
        successTransactionAdapter = new MtSuccessAdapter(successTransactionList,SendMoneyActivity.this,"Mt Transaction");
        RecyclerView.LayoutManager LayoutManager = new LinearLayoutManager(SendMoneyActivity.this);
        recyclerSuccessTransactionList.setLayoutManager(LayoutManager);
        recyclerSuccessTransactionList.setItemAnimator(new DefaultItemAnimator());
        recyclerSuccessTransactionList.setAdapter(successTransactionAdapter);
        txt_msg = (TextView) findViewById(R.id.txt_msg);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                transfer_amount=edt_amt.getText().toString();
                int  selectedValueId = rg_trans_type.getCheckedRadioButtonId();
                //checking the id of the selected radio
                if(selectedValueId == rb_neft.getId())
                {
                    trans_type="neft";
                }
                else if(selectedValueId == rb_imps.getId())
                {
                    trans_type="imps";
                }else{
                    trans_type="";
                }

                if (checkConnection()) {
                    boolean isError=false;
                    if(null==trans_type||trans_type.length()==0||trans_type.equalsIgnoreCase(""))
                    {
                        isError=true;
                        rb_neft.setError("Field Cannot be Blank");
                    }
                    if(null==transfer_amount||transfer_amount.length()<3||transfer_amount.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_amt.setError("Invalid Amount");
                        if(transfer_amount.length()<3)
                        {
                            edt_amt.setError("Amount must be equal or greater than Rs.100");
                        }
                    }
                    if(!isError)
                    {
                        custdialog();
                       /* if (Integer.parseInt(transfer_amount.toString()) <= 25000  )
                        {
                            int limit = Integer.parseInt(neftLimitRs.toString());
                            //Log.d("neftlinit", limit + "");
                            if (Integer.parseInt(transfer_amount.toString()) <= limit )
                            {
                                custdialog();
                            }
                            else
                            {
                                txt_error.setText("Customer transfer limit exceeded for this month");
                            }
                        }
                        else{
                            txt_error.setText("Amount Should be less than 25000");
                        }*/
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        btn_home=(Button)  findViewById(R.id.btn_home);
        btn_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent2=new Intent(getApplicationContext(), HomeActivity.class);
                startActivity(intent2);
                finish();
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class SendMoney extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("amount" , transfer_amount);
                parameters.put("senderId", cust_id);
                parameters.put("beneficiaryId", beneficiaryId);
                parameters.put("remitType", trans_type);
                parameters.put("request_origin", "app");
                parameters.put("totalSurcharge",token);
                parameters.put("senderPhone", sender_no);
                /*parameters.put("senderPhone", sender_no);
                parameters.put("senderName", senderName);
                parameters.put("totalAmount", netAmount);
                parameters.put("processingFee" , processingFee);*/
                this.response = new JSONObject(service.POST(DefineData.SEND_MONEY,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("sendMoneyLog",response+" "+transfer_amount+" "+cust_id+" "+beneficiaryId+" "+trans_type+" "+sender_no+" "+DefineData.SEND_MONEY+" ");
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    String msge="";
                    msge = response.getString("responseDesc");
                    //Log.d("dhds",msge+" ");
                    errMsg= msge;
                    Log.d("tdfetdft",response+"");
                    if(error){
                        String insideError="";
                        if(response.has("insideError")) {
                            insideError = response.getString("insideError");
                            contentTransactionDetails.setVisibility(View.VISIBLE);
                            if (insideError == "true")
                            {
                                Log.d("sgdysh","sdvyfsyufuy");
                                contentTransactionDetails.setVisibility(View.VISIBLE);
                                JSONArray jsonArray=response.getJSONArray("transactionDetails");
                                if(jsonArray.length()!=0) {
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        Item superHero = null;
                                        JSONObject json2 = null;
                                        try {
                                            json2 = jsonArray.getJSONObject(i);
                                            String no = json2.getString("no");
                                            String amount = json2.getString("amount");
                                            String bankRefNo = json2.getString("bankRefNo");
                                            String transactionId = json2.getString("transactionId");
                                            String status = json2.getString("status");
                                            superHero = new Item("Transaction: "+no, "TransactionId: "+transactionId,"₹ " + amount,"Bank Ref. No.: "+bankRefNo, status);
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        //Adding the superhero object to the list
                                        transactionDetailsList.add(superHero);
                                    }
                                    transactionDetailsAdapter.notifyDataSetChanged();
                                }
                            }
                            else
                            {
                                Log.d("fgc","yfyfyufyu");
                                contentTransactionDetails.setVisibility(View.GONE);
                            }
                        }
                    }
                    if(error) {

                        final Integer responseCode=response.getInt("responseCode");
                        final String message = response.getString("responseDesc");
                        //Log.d("errormsg",message+" ");
                        final AlertDialog.Builder builder = new AlertDialog.Builder(SendMoneyActivity.this);
                        builder.setTitle("ERROR");
                        builder.setMessage(message+"");
                        builder.setPositiveButton("Ok",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                        if (responseCode==1057) {

                                            /*JSONObject json=response.getJSONObject("data");
                                            sender_id = json.getString("sender_id");*/
                                            final AlertDialog.Builder builder = new AlertDialog.Builder(SendMoneyActivity.this);
                                            builder.setTitle("Re-Verify Customer");
                                            builder.setMessage("OTP has send on registered mobile number");
                                            builder.setPositiveButton("Ok",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int which) {
                                                            dialog.dismiss();
                                                            //new VerifyCustomer().execute();
                                                            showChangeLangDialog(false,"");
                                                        }
                                                    });
                                            builder.setNegativeButton("Cancel",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog,
                                                                            int which) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                            builder.show();

                                        }
                                        else
                                        {
                                            Log.d("dsfhsgfh","regfeyrf");
                                            Intent i=new Intent(SendMoneyActivity.this, HomeActivity.class);
                                            startActivity(i);
                                            finish();
                                        }
                                    }
                                });
                        builder.show();
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                        //final String insideError = response.getString("insideError");
                    }
                     else {
                        SendMoney.setVisibility(View.GONE);
                        SuccessPage.setVisibility(View.VISIBLE);
                        String msg=response.getString("responseDesc");
                        txt_msg.setText(msg+"");
                      /*  Intent i=new Intent(SendMoneyActivity.this, SuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","Money Transfer");
                        startActivity(i);
                        finish();*/
                        JSONArray jsonArray=response.getJSONArray("transactionDetails");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    json2 = jsonArray.getJSONObject(i);
                                    String no = json2.getString("no");
                                    String amount = json2.getString("amount");
                                    String bankRefNo = json2.getString("bankRefNo");
                                    String transactionId = json2.getString("transactionId");
                                    String status = json2.getString("status");
                                    superHero = new Item("Transaction: "+no, "TransactionId: "+transactionId,"₹ " + amount,"Bank Ref. No.: "+bankRefNo, status);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //Adding the superhero object to the list
                                successTransactionList.add(superHero);
                            }
                            successTransactionAdapter.notifyDataSetChanged();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText(errMsg+" ");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Something went wrong on server");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    private void showChangeLangDialog(boolean isError, String msg) {
        final View dialogView = View.inflate(SendMoneyActivity.this,R.layout.custom_otp_dialog,null);

        final Dialog dialog = new Dialog(SendMoneyActivity.this,R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        final EditText edt_otp=(EditText)dialog.findViewById(R.id.input_otp);
        final TextView txt_Error=(TextView) dialog.findViewById(R.id.serverError);
        Button btn_verify = (Button) dialog.findViewById(R.id.btn_verify);
        Button btn_resend = (Button) dialog.findViewById(R.id.btn_resend);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        if(isError)
        {

            //txt_Error.setError(errorMsg+"");
            txt_Error.setText(errorMsg+"");
        }

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_up=edt_otp.getText().toString();
                //dialog.dismiss();
                //Log.d("customeridscommm",sender_id+" "+otp_up);
                //new Verifyotp().execute();
                if (checkConnection()) {
                    boolean isError=false;

                    if(null==otp_up||otp_up.length()==0||otp_up.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_otp.setError("Field Cannot be Blank");
                    }
                    if(!isError)
                    {
                        new Verifyotp().execute();
                    }

                }else
                {
                   /* txt_Error.setVisibility(View.VISIBLE);
                    txt_Error.setText("Internet Not Available");*/
                }
            }
        });

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new VerifyCustomer().execute();
            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //onBackPressed();
            }
        });

        dialog.show();

    }

    private class Verifyotp extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            //rel_img_bg.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
            //rel_no_records.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("sender_id",sender_id);
                parameters.put("otp",otp_up);
                this.response = new JSONObject(service.POST(DefineData.VERIFY_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                        errorMsg = msg;
                        //Log.d("fourMsg",errorMsg+"");
                    }
                    if (responseCode==1)
                    {
                        //Log.d("otpsend","OTP Send Successfully");
                        JSONObject json=response.getJSONObject("data");
                        mobile_number = json.getString("senderMobileNo");
                        //edt_search_num.setText(mobile_number+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        Intent i=new Intent(SendMoneyActivity.this, SendMoneyActivity.class);
                        startActivity(i);
                        finish();
                    }
                    else{
                        showChangeLangDialog(true,msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class VerifyCustomer extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobileNo",sender_id);
                this.response = new JSONObject(service.POST(DefineData.RESEND_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("verifycustomer","OTP resend");
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    if (responseCode==1)
                    {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);

                        showChangeLangDialog(false,"");
                    }
                    else{
                        //edt_search_num.setText("Error Code : "+responseCode);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    private class FetchDetails extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("senderPhone", sender_no);
                parameters.put("senderId", cust_id);
                parameters.put("senderName", senderName);
                parameters.put("remitType", trans_type);
                parameters.put("amount", transfer_amount);
                parameters.put("beneficiaryId", beneficiaryId);
                //Log.d("oio",sender_no+" "+ cust_id+ " " +senderName + " " +trans_type + " "+transfer_amount+" "  + beneficiaryId+" "+DefineData.FETCH_DETAILS);
                this.response = new JSONObject(service.POST(DefineData.FETCH_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            //Log.d("rtrrtt", response + "");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                        txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                    } else {
                        if(response.length()!=0) {
                            //Item superHero = null;
                            JSONObject json2 = null;
                            try {
                                //Getting json
                                json2 = response.getJSONObject("data");
                                surcharge = json2.getString("surcharge");
                                amount = json2.getString("amount");
                                netAmount = json2.getString("netAmount");
                                processingFee = json2.getString("processingFee");
                                totalAmount = json2.getString("totalAmount");
                                //beneficiaryName = json2.getString("beneficiaryName");
                                //bankName1 = json2.getString("bankName");
                                //accountNo = json2.getString("accountNo");
                                linear_container.setVisibility(View.VISIBLE);
                                progress_linear.setVisibility(View.GONE);
                                custdialog();

                            } catch (JSONException e) {
                                e.printStackTrace();
                                txt_error.setText("Error in parsing");
                                linear_container.setVisibility(View.GONE);
                                progress_linear.setVisibility(View.GONE);
                            }
                            //Adding the superhero object to the list

                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setTextColor(ContextCompat.getColor(getApplicationContext(),R.color.status_fail));
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Empty response from server");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }

        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        hideKeyboard(SendMoneyActivity.this);
        finish();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    private void custdialog(){
        final View dialogView = View.inflate(SendMoneyActivity.this,R.layout.money_trans_confirm_page,null);
        final Dialog dialog = new Dialog(SendMoneyActivity.this,R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        TextView txt_amt=(TextView)dialog.findViewById(R.id.txt_amt);
        TextView txt_trans_type=(TextView)dialog.findViewById(R.id.txt_trans_type);
        TextView txt_bname=(TextView)dialog.findViewById(R.id.txt_bname);
        TextView txt_bank_name=(TextView)dialog.findViewById(R.id.txt_bank_name);
        TextView txt_acctno=(TextView)dialog.findViewById(R.id.txt_acctno);
        TextView txt_mgs=(TextView)dialog.findViewById(R.id.txt_mgs);
        TextView txt_totalAmount=(TextView)dialog.findViewById(R.id.txt_totalAmount);
        TextView txt_surcharge=(TextView)dialog.findViewById(R.id.txt_surcharge);
        TextView txt_neft_error=(TextView)dialog.findViewById(R.id.txt_neft_error);
        String types="";
        //Log.d("shdgf", trans_type);
        if(trans_type.equalsIgnoreCase("neft")) {

            //txt_mgs.setVisibility(View.VISIBLE);
            txt_neft_error.setVisibility(View.VISIBLE);
            types="neft";
        }else{
            //txt_mgs.setVisibility(View.GONE);
            txt_neft_error.setVisibility(View.GONE);
            types="imps";
        }

        Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        txt_trans_type.setText(types);
        txt_amt.setText("₹ "+transfer_amount);
        txt_bname.setText(beneficiaryName);
        txt_bank_name.setText(bankName1);
        txt_acctno.setText(acctNo);
        txt_surcharge.setText("₹ "+surcharge);
        txt_totalAmount.setText("₹ "+totalAmount);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new SendMoney().execute();
            }
        });
        dialog.show();
    }

    private void TransactionListDisplay() {
        final View dialogView = View.inflate(SendMoneyActivity.this,R.layout.mtsuccess_activity,null);
        final Dialog dialog = new Dialog(SendMoneyActivity.this,R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        RecyclerView recyclerView = (RecyclerView) dialog.findViewById(R.id.recyclerView);
        MtSuccessAdapter mAdapter = new MtSuccessAdapter(TransactionList,SendMoneyActivity.this,"Mt Transaction");
        recyclerView.addItemDecoration(new DividerItemDecoration(SendMoneyActivity.this));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(SendMoneyActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        dialog.show();
    }
}

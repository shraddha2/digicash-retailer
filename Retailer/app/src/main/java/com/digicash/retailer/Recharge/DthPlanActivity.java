package com.digicash.retailer.Recharge;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.Model.DividerItemDecoration;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DthPlanActivity extends AppCompatActivity implements View.OnClickListener{

    private List<Item> movieList = new ArrayList<>();
    private List<Item> movieList2 = new ArrayList<>();
    private RecyclerView recyclerView;
    private DthPlanAdapter  mAdapter;
    String token,request_type="Plan";
    SharedPreferences sharedpreferences;
    TextView txt_title,txt_label1,txt_err_msgg,txt_network_msg,txt_label2;
    LinearLayout progress_linear,linear_container;
    RelativeLayout rel_no_records,rel_no_internet;
    String operator_id="",cust_number="";
    private Button[] btn = new Button[2];
    private Button btn_unfocus;
    private int[] btn_id = {R.id.btn_plan, R.id.btn_add_on_pack};
    String OneMONTHS, ThreeMONTHS, SixMONTHS, OneYEAR;

    public DthPlanActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_dth_plan);

        for(int i = 0; i < btn.length; i++){
            btn[i] = (Button) findViewById(btn_id[i]);
            btn[i].setBackgroundColor(Color.rgb(255,255,255));
            btn[i].setOnClickListener(this);
        }

        operator_id=getIntent().getStringExtra("operator_id");
        cust_number=getIntent().getStringExtra("cust_number");
        Log.d("bundle123456",operator_id+" "+cust_number+" ");

        btn_unfocus = btn[0];

        recyclerView = (RecyclerView) findViewById(R.id.rc_plan_history);
        rel_no_records= (RelativeLayout) findViewById(R.id.rel_no_records);
        txt_err_msgg= (TextView) findViewById(R.id.txt_err_msgg);
        linear_container= (LinearLayout) findViewById(R.id.container);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijiCashRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        rel_no_records.setVisibility(View.GONE);
        linear_container.setVisibility(View.GONE);

        recyclerView.addItemDecoration(new DividerItemDecoration(this));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        setFocus(btn_unfocus, btn[0]);
        if(checkConnection()) {
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
            new ViewPlan().execute();
            mAdapter = new DthPlanAdapter(movieList,this);
            recyclerView.setAdapter(mAdapter);
        }else{
            linear_container.setVisibility(View.GONE);
            rel_no_records.setVisibility(View.GONE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btn_plan :
                request_type="Plan";
                setFocus(btn_unfocus, btn[0]);
                mAdapter = new DthPlanAdapter(movieList,this);
                recyclerView.setAdapter(mAdapter);
                break;
            case R.id.btn_add_on_pack :
                request_type="Add-On Pack";
                setFocus(btn_unfocus, btn[1]);
                mAdapter = new DthPlanAdapter(movieList2,this);
                recyclerView.setAdapter(mAdapter);
                break;
        }

      /*  if(checkConnection()) {
            rel_no_records.setVisibility(View.GONE);
            new ViewPlan().execute();
        }else{
            rel_no_records.setVisibility(View.GONE);
        }*/
    }

    private void setFocus(Button btn_unfocus, Button btn_focus){
        btn_unfocus.setTextColor(Color.rgb(49, 50, 51));
        btn_unfocus.setBackgroundColor(Color.rgb(255,255,255));
        btn_focus.setTextColor(Color.rgb(255,255,255));
        btn_focus.setBackgroundColor(Color.rgb(255, 155, 0));
        this.btn_unfocus = btn_focus;
    }

    private class ViewPlan extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            movieList.clear();
            movieList2.clear();
            rel_no_records.setVisibility(View.GONE);
            linear_container.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId", operator_id);
                parameters.put("vcNumber", cust_number);
                this.response = new JSONObject(service.POST(DefineData.VIEW_DTH_PLANS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    JSONObject resjson = response.getJSONObject("records");
                    JSONArray jsonArray=resjson.getJSONArray("Plan");
                    JSONArray jsonArray2=resjson.getJSONArray("Add-On Pack");
//                    if (request_type.equalsIgnoreCase("Plan")) {
                        if (jsonArray.length() != 0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    json2 = jsonArray.getJSONObject(i);
                                    JSONObject rs = json2.getJSONObject("rs");
                                    StringBuilder rss = new StringBuilder();

                                    if (rs.has("1 MONTHS")) {
                                        OneMONTHS = rs.getString("1 MONTHS");
                                    }
                                    if (rs.has("3 MONTHS")) {
                                        ThreeMONTHS = rs.getString("3 MONTHS");
                                    }
                                    if (rs.has("6 MONTHS")) {
                                        SixMONTHS = rs.getString("6 MONTHS");                                    }

                                    if (rs.has("1 YEAR")) {
                                        OneYEAR = rs.getString("1 YEAR");
                                    }
                                    String desc = json2.getString("desc");
                                    String plan_name = json2.getString("plan_name");
                                    String last_update = json2.getString("last_update");
                                    superHero = new Item(plan_name, desc, last_update, OneMONTHS, ThreeMONTHS, SixMONTHS, OneYEAR);
                                    //Log.d("operatorid",rs+" "+ desc+" "+validity+" "+last_update+" ");

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setVisibility(View.VISIBLE);
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    //progress_linear.setVisibility(View.GONE);
                                }
                                movieList.add(superHero);
                            }
                        }
                    //}
                    /*if (request_type.equalsIgnoreCase("Add-On Pack")) {*/

                        if (jsonArray2.length() != 0) {
                            for (int j = 0; j < jsonArray2.length(); j++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    json2 = jsonArray2.getJSONObject(j);

                                    JSONObject rs = json2.getJSONObject("rs");

                                    if (rs.has("1 MONTHS")) {
                                        OneMONTHS = rs.getString("1 MONTHS");
                                    }
                                    if (rs.has("3 MONTHS")) {
                                        ThreeMONTHS = rs.getString("3 MONTHS");
                                    }
                                    if (rs.has("6 MONTHS")) {
                                        SixMONTHS = rs.getString("6 MONTHS");
                                    }
                                    if (rs.has("1 YEAR")) {
                                        OneYEAR = rs.getString("1 YEAR");
                                    }
                                    String desc = json2.getString("desc");
                                    String plan_name = json2.getString("plan_name");
                                    String last_update = json2.getString("last_update");
                                    superHero = new Item(plan_name, desc, last_update, OneMONTHS, ThreeMONTHS, SixMONTHS, OneYEAR);
                                    movieList2.add(superHero);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txt_err_msgg.setVisibility(View.VISIBLE);
                                    txt_err_msgg.setText("Error in parsing response");
                                    linear_container.setVisibility(View.VISIBLE);
                                    //progress_linear.setVisibility(View.GONE);
                                }
                            }
                        }
                    //}

                    linear_container.setVisibility(View.VISIBLE);
                    rel_no_records.setVisibility(View.GONE);

                    //}
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_err_msgg.setVisibility(View.VISIBLE);
                    txt_err_msgg.setText("In Catch---Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                }
            }else{
                txt_err_msgg.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
            }
        }
    }


    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}

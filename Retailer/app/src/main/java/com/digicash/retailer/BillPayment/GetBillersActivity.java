package com.digicash.retailer.BillPayment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.Model.DividerItemDecoration;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GetBillersActivity extends AppCompatActivity {

    ConstraintLayout loading,billOperatorListContent;
    RecyclerView recyclerBillOperator;
    SharedPreferences sharedpreferences;
    String token="";
    TextView txtError;

    private List<Item> BillList = new ArrayList<>();
    private RecyclerView recyclerView;
    private GetBillerAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_billers);

        getSupportActionBar().setTitle("Operator List");
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijiCashRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        loading=(ConstraintLayout) findViewById(R.id.loading);
        billOperatorListContent=(ConstraintLayout) findViewById(R.id.billOperatorListContent);
        txtError= (TextView) findViewById(R.id.txtError);

        recyclerBillOperator = (RecyclerView) findViewById(R.id.recyclerBillOperator);

        mAdapter = new GetBillerAdapter(BillList,GetBillersActivity.this);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerBillOperator.setLayoutManager(mLayoutManager);
        recyclerBillOperator.addItemDecoration(new DividerItemDecoration(GetBillersActivity.this));
        recyclerBillOperator.setItemAnimator(new DefaultItemAnimator());
        recyclerBillOperator.setAdapter(mAdapter);

        if(checkConnection()) {
            billOperatorListContent.setVisibility(View.GONE);
            loading.setVisibility(View.GONE);
            new FetchBillOperatorList().execute();
        }else{
            billOperatorListContent.setVisibility(View.GONE);
            loading.setVisibility(View.GONE);
        }
    }

    private class FetchBillOperatorList extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            BillList.clear();
            billOperatorListContent.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{

                this.response = new JSONObject(service.POST(DefineData.GET_BILLERS,token));

            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("cdccv",response+"");
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txtError.setText(msg+"");
                        billOperatorListContent.setVisibility(View.GONE);
                        loading.setVisibility(View.GONE);
                    } else {
                        JSONArray jsonArray=response.getJSONArray("data");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {
                                    json2 = jsonArray.getJSONObject(i);
                                    String id = json2.getString("id");
                                    String bbps_operator_id = json2.getString("bbps_operator_id");
                                    String bbps_operator_name = json2.getString("bbps_operator_name");
                                    superHero = new Item(id,bbps_operator_id,bbps_operator_name);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                    txtError.setText("Oops! Something Went Wrong");
                                    billOperatorListContent.setVisibility(View.GONE);
                                    loading.setVisibility(View.GONE);
                                }
                                //Adding the superhero object to the list
                                BillList.add(superHero);
                            }
                            mAdapter.notifyDataSetChanged();
                            billOperatorListContent.setVisibility(View.VISIBLE);
                            loading.setVisibility(View.GONE);
                        }else{
                            txtError.setText("Something Went Wrong! Try Again Later");
                            billOperatorListContent.setVisibility(View.GONE);
                            loading.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setText("Oops! Something Went Wrong");
                    billOperatorListContent.setVisibility(View.GONE);
                    loading.setVisibility(View.GONE);
                }
            }else{
                txtError.setText("Something Went Wrong! Try Again Later");
                billOperatorListContent.setVisibility(View.GONE);
                loading.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.bill_list_menu, menu);
        MenuItem searchViewItem = menu.findItem(R.id.action_search);
        //final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchViewItem);
        SearchView searchView = (SearchView) searchViewItem.getActionView();
        search(searchView);

        return super.onCreateOptionsMenu(menu);
    }

    private void search(SearchView searchView) {
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mAdapter.getFilter().filter(newText);
                return true;
            }
        });
    }

    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent=new Intent();
        setResult(5,intent);
        finish();//finishing activity

    }
}
package com.digicash.retailer.SendMoney;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by shraddha on 09-07-2018.
 */

public class OperatorCircleAdapter extends RecyclerView.Adapter<OperatorCircleAdapter.MyViewHolder> implements Filterable {

    List<Item> billOpertaorList = Collections.emptyList();
    private List<Item> mFilteredList;
    Context context;

    public OperatorCircleAdapter(List<Item> billOpertaorList, Context context) {
        this.billOpertaorList = billOpertaorList;
        this.context = context;
        this.mFilteredList = billOpertaorList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtBankName,txtBankId;

        public MyViewHolder(View view) {
            super(view);
            txtBankId = (TextView) view.findViewById(R.id.txtBankId);
            txtBankName = (TextView) view.findViewById(R.id.txtBankName);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id=txtBankId.getText().toString();
                    String operator_name=txtBankName.getText().toString();
                    Intent intent=new Intent(context, BankNameListActivity.class);
                    intent.putExtra("BankId",id);
                    intent.putExtra("BankName",operator_name);
                    ((Activity) context).setResult(3,intent);
                    ((Activity) context).finish();//finishing activity
                }
            });
        }
    }

    @Override
    public OperatorCircleAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.bank_list_layout, parent, false);

        return new OperatorCircleAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = billOpertaorList.get(position);
        holder.txtBankId.setText(mFilteredList.get(position).getOperator_name()+"");
        holder.txtBankName.setText(mFilteredList.get(position).getCommission()+"");
    }

    @Override
    public int getItemCount()
    {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = billOpertaorList;
                } else {

                    ArrayList<Item> filteredList = new ArrayList<>();

                    for (Item androidVersion : billOpertaorList) {

                        if (androidVersion.getCommission().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
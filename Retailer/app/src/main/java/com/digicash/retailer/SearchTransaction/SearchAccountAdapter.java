package com.digicash.retailer.SearchTransaction;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.AsyncTask;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.List;

public class SearchAccountAdapter extends RecyclerView.Adapter<SearchAccountAdapter.MyViewHolder> {
    // Typeface font ;
    private List<Item> moviesList;
    Context ctx;
    String frg_name="",token="";
    SharedPreferences sharedpreferences;
    String orderId="";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8,txt_label9,txt_label10,txt_label11,textView70,txt_label12,txt_label13;
        ImageView img_arrow;
        Button img_fail_icon,img_check_icon;
        ConstraintLayout linear_more_data,progress_linear;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
            txt_label9 = (TextView) view.findViewById(R.id.txt_label9);
            txt_label10 = (TextView) view.findViewById(R.id.txt_label10);
            txt_label11 = (TextView) view.findViewById(R.id.txt_label11);
            textView70 = (TextView) view.findViewById(R.id.textView70);
            txt_label12 = (TextView) view.findViewById(R.id.txt_label12);
            txt_label13 = (TextView) view.findViewById(R.id.txt_label13);
            img_fail_icon= (Button) view.findViewById(R.id.img_fail_icon);
            img_check_icon= (Button) view.findViewById(R.id.img_check_icon);
            img_arrow= (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data= (ConstraintLayout) view.findViewById(R.id.linear_more_data);
            progress_linear= (ConstraintLayout) view.findViewById(R.id.loading);

            progress_linear.setVisibility(View.GONE);

            img_fail_icon.setVisibility(View.GONE);
            img_check_icon.setVisibility(View.GONE);
            //txt_label3.setVisibility(View.GONE);

            if(frg_name.equalsIgnoreCase("Transaction History"))
            {
                img_fail_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        final String trans_id=txt_label1.getText().toString();
                        final AlertDialog.Builder builder = new AlertDialog.Builder(
                                ctx);
                        builder.setTitle("Confirmation");
                        builder.setMessage("Are you sure you want to refund the amount?");
                        builder.setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        new FetchMTLastTrans().execute(trans_id);

                                    }
                                });
                        builder.setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }
                });

                //img_check_icon.setImageDrawable(ContextCompat.getDrawable(ctx,R.drawable.ic_revert));
                img_check_icon.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        orderId=txt_label3.getText().toString();
                        final AlertDialog.Builder builder = new AlertDialog.Builder(
                                ctx);
                        builder.setMessage("Check Status?");
                        builder.setPositiveButton("YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        new CheckStatus().execute();

                                    }
                                });
                        builder.setNegativeButton("NO",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,
                                                        int which) {
                                        dialog.dismiss();
                                    }
                                });
                        builder.show();
                    }
                });
            }


            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility =linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE){
                        if(frg_name.equalsIgnoreCase("Wallet History")) {
                            txt_label3.setVisibility(View.GONE);
                        }
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    }else{
                        if(frg_name.equalsIgnoreCase("Wallet History")) {
                            txt_label3.setVisibility(View.VISIBLE);
                        }
                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public SearchAccountAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
        this.sharedpreferences = ctx.getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        this.token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        // font = Typeface.createFromAsset(ctx.getAssets(), "font/Montserrat-Regular.ttf");
    }

    @Override
    public SearchAccountAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.search_ac_no_adapter_layout, parent, false);

        return new SearchAccountAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SearchAccountAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getLabel31()+"");
        holder.txt_label2.setText("₹ "+movie.getLabel32()+"");
        holder.txt_label3.setText(movie.getLabel33()+"");
        holder.txt_label4.setText(movie.getLabel34()+"");
        holder.txt_label5.setText(movie.getLabel35()+"");
        holder.txt_label6.setText(movie.getLabel36()+"");
        holder.txt_label7.setText(DefineData.parseDateToddMMyyyyhh(movie.getLabel37()) + "");
        holder.txt_label8.setText(movie.getLabel38()+"");
        holder.txt_label9.setText(movie.getLabel39()+"");
        holder.txt_label10.setText(movie.getLabel40()+"");
        holder.txt_label11.setText(movie.getLabel41()+"");
        holder.textView70.setText(movie.getLabel42()+"");
        holder.txt_label12.setText(movie.getLabel43()+"");
        holder.txt_label13.setText(movie.getLabel45()+"");

        if(frg_name.equalsIgnoreCase("Search Account Number")) {

            if (movie.getLabel38().equalsIgnoreCase("failed")) {
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_fail));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_fail));
                holder.img_fail_icon.setVisibility(View.GONE);
                holder.img_check_icon.setVisibility(View.GONE);
            } else if (movie.getLabel38().equalsIgnoreCase("pending")){
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_initiated));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_initiated));
                holder.img_fail_icon.setVisibility(View.GONE);
                holder.img_check_icon.setVisibility(View.GONE);

            }else if (movie.getLabel38().equalsIgnoreCase("response Pending")){
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_initiated));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_initiated));
                holder.img_fail_icon.setVisibility(View.GONE);
                holder.img_check_icon.setVisibility(View.GONE);
            }else{
                holder.txt_label2.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_success));
                holder.txt_label8.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(),R.color.status_success));
                holder.img_fail_icon.setVisibility(View.GONE);
                holder.img_check_icon.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }


    private class FetchMTLastTrans extends AsyncTask<String, Void, Void> {
        ProgressBar progressBar = new ProgressBar(ctx, null, android.R.attr.progressBarStyleHorizontal);
        String trans_id;
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(String... params) {
            trans_id=params[0];
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("transactionId", trans_id);
                this.response = new JSONObject(service.POST(DefineData.MT_REFUND_MONEY_TRANSFER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {

            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {

                    } else {

                        custdialog(trans_id);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }
        }
    }

    private void  custdialog(final String trans_id) {

        final View dialogView = View.inflate(ctx,R.layout.custom_otp_dialog,null);

        final Dialog dialog = new Dialog(ctx,R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        final EditText edt_otp=(EditText)dialog.findViewById(R.id.input_otp);
        Button btn_resend = (Button) dialog.findViewById(R.id.btn_resend);
        Button btn_submit = (Button) dialog.findViewById(R.id.btn_verify);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        btn_resend.setVisibility(View.GONE);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();

            }
        });

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_customer_otp=edt_otp.getText().toString();
                dialog.dismiss();
                new RefundBalance().execute(trans_id,new_customer_otp);
            }
        });

        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

        dialog.show();
    }

    private class RefundBalance extends AsyncTask<String, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(String... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("transactionId", params[0]);
                parameters.put("otp", params[1]);
                this.response = new JSONObject(service.POST(DefineData.MT_REFUND_SUBMIT_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            String msg="";
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        msg=response.getString("data");
                    } else {
                        msg=response.getString("data");
                    }
                    Toast.makeText(ctx,msg+"", Toast.LENGTH_LONG).show();
                    Intent i=new Intent(ctx, HomeActivity.class);
                    ctx.startActivity(i);
                    ((Activity)ctx).finish();
                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }

        }
    }

    private class CheckStatus extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("orderId", orderId+"");
                this.response = new JSONObject(service.POST(DefineData.CHECK__PENDING_STATUS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            String msg="";
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        msg=response.getString("data");
                    } else {
                        msg=response.getString("data");
                    }
                    Toast.makeText(ctx,msg+"", Toast.LENGTH_LONG).show();
                    Intent i=new Intent(ctx, HomeActivity.class);
                    ctx.startActivity(i);
                    ((Activity)ctx).finish();

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }else{

            }

        }
    }
}

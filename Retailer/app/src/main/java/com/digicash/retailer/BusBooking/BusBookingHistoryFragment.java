package com.digicash.retailer.BusBooking;

import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.digicash.retailer.Model.DividerItemDecoration;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;

import java.util.ArrayList;
import java.util.List;

public class BusBookingHistoryFragment extends Fragment {

    private List<Item> movieList = new ArrayList<>();
    private RecyclerView recyclerView;
    private BusBookingHistoryAdapter mAdapter;
    LinearLayout linear_container;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_bus_booking_history, container, false);

        linear_container= (LinearLayout) rootView.findViewById(R.id.container);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.rc_bus_booking);

        linear_container.setVisibility(View.VISIBLE);


        mAdapter = new BusBookingHistoryAdapter(movieList,getActivity(),"Bus Booking History");
        recyclerView.addItemDecoration(new DividerItemDecoration(getActivity()));
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        return rootView;
    }


}

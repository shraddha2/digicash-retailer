package com.digicash.retailer.TransactionHistory;

import android.content.Context;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;

import java.util.List;

public class BillPaymentAdapter extends RecyclerView.Adapter<BillPaymentAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name = "";

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txtOperatorName,txtAmount,txtConsumerId,txtBillPaymentId,txtCustomerPhone,txtSurcharge,txtBillerTransactionId,txtremark,updated_time,txttime,txtStatus;
        ImageView img_arrow;
        ConstraintLayout linear_more_data;

        //Typeface font ;
        public MyViewHolder(View view) {
            super(view);
            txtOperatorName = (TextView) view.findViewById(R.id.txtOperatorName);
            txtAmount = (TextView) view.findViewById(R.id.txtAmount);
            txtConsumerId = (TextView) view.findViewById(R.id.txtConsumerId);
            txtBillPaymentId = (TextView) view.findViewById(R.id.txtBillPaymentId);
            txtCustomerPhone = (TextView) view.findViewById(R.id.txtCustomerPhone);
            txtSurcharge = (TextView) view.findViewById(R.id.txtSurcharge);
            txtBillerTransactionId = (TextView) view.findViewById(R.id.txtBillerTransactionId);
            txtremark = (TextView) view.findViewById(R.id.txtremark);
            updated_time = (TextView) view.findViewById(R.id.updated_time);
            txttime = (TextView) view.findViewById(R.id.txttime);
            txtStatus = (TextView) view.findViewById(R.id.txtStatus);
            img_arrow = (ImageView) view.findViewById(R.id.img_arrow);
            linear_more_data = (ConstraintLayout) view.findViewById(R.id.linear_more_data);

            img_arrow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int visibility = linear_more_data.getVisibility();
                    if (visibility == View.VISIBLE) {
                        linear_more_data.setVisibility(View.GONE);
                        img_arrow.setImageResource(R.drawable.ic_expand_more);
                    } else {

                        linear_more_data.setVisibility(View.VISIBLE);
                        img_arrow.setImageResource(R.drawable.ic_expand_less);
                    }
                }
            });
        }
    }

    public BillPaymentAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public BillPaymentAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.bill_payment_layout, parent, false);

        return new BillPaymentAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(BillPaymentAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txtOperatorName.setText(movie.getId() + "");
        holder.txtAmount.setText(movie.getAmount() + "");
        holder.txtConsumerId.setText("Consumer Id: " + movie.getSurcharge());
        holder.txtBillPaymentId.setText("Bill Payment Id: " + movie.getTime() + "");
        holder.txtCustomerPhone.setText("Customer No: "+movie.getMTStatus() + "");
        holder.txtSurcharge.setText("Surcharge: " + movie.getSender() + "");
        holder.txtBillerTransactionId.setText("Biller Transaction Id: "+movie.getBeneficiaryName() + "");
        holder.txtremark.setText("Remark: "+movie.getRecipientAccount() + "");
        holder.updated_time.setText("Updated Time: "+movie.getRecipientMobile()+"");
        holder.txttime.setText(movie.getRecipientBank() +"");
        holder.txtStatus.setText(movie.getBankRefNo() + "");

        if (movie.getBankRefNo().equalsIgnoreCase("Failed")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_fail));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_fail));
        } else if (movie.getBankRefNo().equalsIgnoreCase("Pending")) {
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_initiated));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_initiated));
        } else {
            holder.txtAmount.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_success));
            holder.txtStatus.setTextColor(ContextCompat.getColor(DijiCashRetailer.getInstance(), R.color.status_success));
        }
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

package com.digicash.retailer.Model;

/**
 * Created by sai116 on 11/15/2017.
 */

public class Item implements Comparable<Item> {
    public int imageId,label44;
    public String txt,txt_id;
    public String operator_name,commission,sale_amt, limitLeft,profit_amt,p_name,acno;
    public String label1,label2,label3,label4,label5,label6,label7,label8,label9,dattime;
    public String label11,label12,label13,label14,label15,label16,label17,label18,label19,label20,label21,label22,label23;
    public String trans_no,trans_type,trans_amount,trans_datetime,trans_update_balance,status,comment;
    public String title,type,amt,balance;
    public String Id,Amount,Surcharge,Time,MTStatus,Sender,BeneficiaryName,RecipientAccount,RecipientMobile,RecipientBank,BankRefNo;
    int credit_type;

    public Item()
    {}

    public String getTitle() {
        return title;
    }

    public Item(String title, String type, String amt, String balance, int credit_type, String dattime) {
        this.title=title;
        this.type=type;
        this.amt=amt;
        this.balance=balance;
        this.credit_type=credit_type;
        this.dattime=dattime;
    }


    public Item(String label1,String label2,String label3,String label4,String label5) {
        this.label1=label1;
        this.label2=label2;
        this.label3=label3;
        this.label4=label4;
        this.label5=label5;
    }

    public Item(String label1, String label2,String label3,String label4,String label5,String label6,String label7,String label8,String label9) {
        this.label1=label1;
        this.label2=label2;
        this.label3=label3;
        this.label4=label4;
        this.label5=label5;
        this.label6=label6;
        this.label7=label7;
        this.label8=label8;
        this.label9=label9;
    }

    public void setTxt(String txt) {
        this.txt = txt;
    }

    public void setTxt_id(String txt_id) {
        this.txt_id = txt_id;
    }

    public Item(String label1, String label2, String label3, String dattime) {
        this.label1=label1;
        this.label2=label2;
        this.label3=label3;
        this.dattime=dattime;

    }
    @Override
    public int compareTo(Item o) {
        return getDattime().compareTo(o.getDattime());
    }
    public Item( int imageId, String text,String txt_id) {

        this.imageId = imageId;
        this.txt=text;
        this.txt_id=txt_id;
    }

    public int getCredit_type() {
        return credit_type;
    }

    public String getAmt() {
        return amt;
    }

    public String getBalance() {
        return balance;
    }

    public String getComment() {
        return comment;
    }

    public String getType() {
        return type;
    }

    public String getDattime() {
        return dattime;
    }

    public void setDattime(String dattime) {
        this.dattime = dattime;
    }

    public String getLabel8() {
        return label8;
    }

    public String getLabel9() {
        return label9;
    }

    public String getLabel1() {
        return label1;
    }

    public String getLabel2() {
        return label2;
    }

    public String getLabel3() {
        return label3;
    }

    public String getLabel4() {
        return label4;
    }

    public String getLabel5() {
        return label5;
    }

    public String getLabel6() {
        return label6;
    }

    public String getLabel7() {
        return label7;
    }

    public String getTxt_id() {
        return txt_id;
    }


    public Item(String operator_name, String commission) {

        this.operator_name = operator_name;
        this.commission=commission;
    }

    public Item( String operator_name, String sale_amt, String profit_amt) {

        this.operator_name = operator_name;
        this.sale_amt=sale_amt;
        this.profit_amt=profit_amt;
    }



    public Item( String label1, String label2, String label3,String label4,String label5,String label6,int a) {

        this.label1 = label1;
        this.label2=label2;
        this.label3=label3;
        this.label4=label4;
        this.label5=label5;
        this.label6=label6;
    }

    public Item(String trans_no, String trans_type,String trans_amount,String trans_datetime,String trans_update_balance,String status) {
        this.trans_no=trans_no;
        this.trans_type=trans_type;
        this.trans_amount=trans_amount;
        this.trans_datetime=trans_datetime;
        this.trans_update_balance=trans_update_balance;
        this.status=status;
    }
    public Item(String trans_no, String trans_type,String trans_amount,String trans_datetime,String trans_update_balance,String status,String p_name) {
        this.trans_no=trans_no;
        this.trans_type=trans_type;
        this.trans_amount=trans_amount;
        this.trans_datetime=trans_datetime;
        this.trans_update_balance=trans_update_balance;
        this.status=status;
        this.p_name=p_name;
    }

    public Item(String trans_no, String trans_type,String trans_amount,String trans_datetime,String trans_update_balance,String status,String p_name,String acno) {
        this.trans_no=trans_no;
        this.trans_type=trans_type;
        this.trans_amount=trans_amount;
        this.trans_datetime=trans_datetime;
        this.trans_update_balance=trans_update_balance;
        this.status=status;
        this.p_name=p_name;
        this.acno=acno;
    }

    public String getAcno() {
        return acno;
    }

    public String getCommission() {
        return commission;
    }

    public String getOperator_name() {
        return operator_name;
    }

    public String getStatus() {
        return status;
    }

    public int getImageId() {
        return imageId;
    }

    public String getTrans_amount() {
        return trans_amount;
    }

    public String getTrans_datetime() {
        return trans_datetime;
    }

    public String getTrans_no() {
        return trans_no;
    }

    public String getTrans_type() {
        return trans_type;
    }

    public String getTrans_update_balance() {
        return trans_update_balance;
    }

    public String getTxt() {
        return txt;
    }

    public String getProfit_amt() {
        return profit_amt;
    }

    public String getSale_amt() {
        return sale_amt;
    }

    public String getP_name() {
        return p_name;
    }

    public Item(String label11, String label12, String label13, String label14, String label15, String label16, String label17, String label18, String label19, String label20) {
        this.label11=label11;
        this.label12=label12;
        this.label13=label13;
        this.label14=label14;
        this.label15=label15;
        this.label16=label16;
        this.label17=label17;
        this.label18=label18;
        this.label19=label19;
        this.label20=label20;
    }

    public String getLabel11() {
        return label11;
    }
    public String getLabel12() {
        return label12;
    }
    public String getLabel13() {
        return label13;
    }
    public String getLabel14() {
        return label14;
    }
    public String getLabel15() {
        return label15;
    }
    public String getLabel16() {
        return label16;
    }
    public String getLabel17() {
        return label17;
    }
    public String getLabel18() {
        return label18;
    }
    public String getLabel19() {
        return label19;
    }
    public String getLabel20() {
        return label20;
    }


    public Item(String Id, String Amount, String Surcharge, String Time, String MTStatus, String Sender, String BeneficiaryName,
                String RecipientAccount, String RecipientMobile, String RecipientBank, String BankRefNo) {
        this.Id=Id;
        this.Amount=Amount;
        this.Surcharge=Surcharge;
        this.Time=Time;
        this.MTStatus=MTStatus;
        this.Sender=Sender;
        this.BeneficiaryName=BeneficiaryName;
        this.RecipientAccount=RecipientAccount;
        this.RecipientMobile=RecipientMobile;
        this.RecipientBank=RecipientBank;
        this.BankRefNo=BankRefNo;
    }

    public String getId() {
        return Id;
    }
    public String getAmount() {
        return Amount;
    }
    public String getSurcharge() {
        return Surcharge;
    }
    public String getTime() {
        return Time;
    }
    public String getMTStatus() {
        return MTStatus;
    }
    public String getSender() {
        return Sender;
    }
    public String getBeneficiaryName() {
        return BeneficiaryName;
    }
    public String getRecipientAccount() {
        return RecipientAccount;
    }
    public String getRecipientMobile() {
        return RecipientMobile;
    }
    public String getRecipientBank() {
        return RecipientBank;
    }
    public String getBankRefNo() {
        return BankRefNo;
    }



    public Item(String label1, String label2, String label3, int label44, String label5, String label6, String label7) {
        this.label1=label1;
        this.label2=label2;
        this.label3=label3;
        this.label44=label44;
        this.label5=label5;
        this.label6=label6;
        this.label7=label7;
    }
    public int getLabel44() {
        return label44;
    }


    public Item(String label11, String label12, String label13, String label14, String label15, String label16, String label17, String label18, String label19, String label20, String label21, String label22, String label23) {
        this.label11=label11;
        this.label12=label12;
        this.label13=label13;
        this.label14=label14;
        this.label15=label15;
        this.label16=label16;
        this.label17=label17;
        this.label18=label18;
        this.label19=label19;
        this.label20=label20;
        this.label21=label21;
        this.label22=label22;
        this.label23=label23;
    }
    public String getLabel21() {
        return label21;
    }
    public String getLabel22() {
        return label22;
    }
    public String getLabel23() {
        return label23;
    }

    public String label31,label32,label33,label34,label35,label36,label37,label38,label39,label40,label41,label42,label43,label45;
    public Item(String label31, String label32, String label33, String label34, String label35, String label36, String label37, String label38, String label39, String label40, String label41, String label42, String label43, String label45) {
        this.label31 = label31;
        this.label32 = label32;
        this.label33 = label33;
        this.label34 = label34;
        this.label35 = label35;
        this.label36 = label36;
        this.label37 = label37;
        this.label38 = label38;
        this.label39 = label39;
        this.label40 = label40;
        this.label41 = label41;
        this.label42 = label42;
        this.label43 = label43;
        this.label45 = label45;
    }

    public String getLabel31() {
        return label31;
    }
    public String getLabel32() {
        return label32;
    }
    public String getLabel33() {
        return label33;
    }
    public String getLabel34() {
        return label34;
    }
    public String getLabel35() {
        return label35;
    }
    public String getLabel36() {
        return label36;
    }
    public String getLabel37() {
        return label37;
    }
    public String getLabel38() {
        return label38;
    }
    public String getLabel39() {
        return label39;
    }
    public String getLabel40() {
        return label40;
    }
    public String getLabel41() {
        return label41;
    }
    public String getLabel42() {
        return label42;
    }
    public String getLabel43() {
        return label43;
    }
    public String getLabel45() {
        return label45;
    }

    public String Label51,Label52,Label53,Label54,Label55,Label56,Label57,Label58,Label59,Label60,Label61,Label62;

    public Item(String label51, String label52, String label53, String label54, String label55, String label56, String label57, String label58, String label59, String label60, String label61, String label62) {
        Label51 = label51;
        Label52 = label52;
        Label53 = label53;
        Label54 = label54;
        Label55 = label55;
        Label56 = label56;
        Label57 = label57;
        Label58 = label58;
        Label59 = label59;
        Label60 = label60;
        Label61 = label61;
        Label62 = label62;
    }

    public String getLabel51() {
        return Label51;
    }
    public String getLabel52() {
        return Label52;
    }
    public String getLabel53() {
        return Label53;
    }
    public String getLabel54() {
        return Label54;
    }
    public String getLabel55() {
        return Label55;
    }
    public String getLabel56() {
        return Label56;
    }
    public String getLabel57() {
        return Label57;
    }
    public String getLabel58() {
        return Label58;
    }
    public String getLabel59() {
        return Label59;
    }
    public String getLabel60() {
        return Label60;
    }
    public String getLabel61() {
        return Label61;
    }
    public String getLabel62() {
        return Label62;
    }
}

package com.digicash.retailer.BillPayment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GetBillerAdapter extends RecyclerView.Adapter<GetBillerAdapter.MyViewHolder> implements Filterable {

    List<Item> billOpertaorList = Collections.emptyList();
    private List<Item> mFilteredList;
    Context context;

    public GetBillerAdapter(List<Item> billOpertaorList, Context context) {
        this.billOpertaorList = billOpertaorList;
        this.context = context;
        this.mFilteredList = billOpertaorList;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtId,txtBbpsOperatorId,txtBbpsOperatorName;
        public MyViewHolder(View view) {
            super(view);

            txtId=(TextView) view.findViewById(R.id.txtId);
            txtBbpsOperatorId=(TextView) view.findViewById(R.id.txtBbpsOperatorId);
            txtBbpsOperatorName=(TextView) view.findViewById(R.id.txtBbpsOperatorName);

            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String id=txtId.getText().toString();
                    String OperatorId=txtBbpsOperatorId.getText().toString();
                    String operatorName=txtBbpsOperatorName.getText().toString();
                    Intent intent=new Intent(context, GetBillersActivity.class);
                    intent.putExtra("getBillerid",id);
                    intent.putExtra("getBillerOperatorId",OperatorId);
                    intent.putExtra("getBillerOperatorName",operatorName);
                    Log.d("fgh",id+" "+OperatorId+" "+operatorName+" ");
                    ((Activity) context).setResult(1,intent);
                    ((Activity) context).finish();//finishing activity
                }
            });
        }
    }

    @Override
    public GetBillerAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.get_biller_layout, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Item movie = billOpertaorList.get(position);
        holder.txtId.setText(mFilteredList.get(position).getOperator_name()+"");
        holder.txtBbpsOperatorId.setText(mFilteredList.get(position).getSale_amt()+"");
        holder.txtBbpsOperatorName.setText(mFilteredList.get(position).getProfit_amt()+"");

    }

    @Override
    public int getItemCount()
    {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {

        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {

                String charString = charSequence.toString();

                if (charString.isEmpty()) {

                    mFilteredList = billOpertaorList;
                } else {

                    ArrayList<Item> filteredList = new ArrayList<>();

                    for (Item androidVersion : billOpertaorList) {

                        if (androidVersion.getProfit_amt().toLowerCase().contains(charString)) {

                            filteredList.add(androidVersion);
                        }
                    }

                    mFilteredList = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Item>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
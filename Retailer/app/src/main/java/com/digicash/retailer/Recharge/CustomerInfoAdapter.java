package com.digicash.retailer.Recharge;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.digicash.retailer.Model.Item;
import com.digicash.retailer.R;

import java.util.List;

public class CustomerInfoAdapter extends RecyclerView.Adapter<CustomerInfoAdapter.MyViewHolder> {

    private List<Item> moviesList;
    Context ctx;
    String frg_name="";
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView txt_label1, txt_label2, txt_label3,txt_label4,txt_label5,txt_label6,txt_label7,txt_label8;

        public MyViewHolder(View view) {
            super(view);
            txt_label1 = (TextView) view.findViewById(R.id.txt_label1);
            txt_label2 = (TextView) view.findViewById(R.id.txt_label2);
            txt_label3 = (TextView) view.findViewById(R.id.txt_label3);
            txt_label4 = (TextView) view.findViewById(R.id.txt_label4);
            txt_label5 = (TextView) view.findViewById(R.id.txt_label5);
            txt_label6 = (TextView) view.findViewById(R.id.txt_label6);
            txt_label7 = (TextView) view.findViewById(R.id.txt_label7);
            txt_label8 = (TextView) view.findViewById(R.id.txt_label8);
        }
    }


    public CustomerInfoAdapter(List<Item> moviesList, Context ctx, String frg_name) {
        this.moviesList = moviesList;
        this.ctx = ctx;
        this.frg_name = frg_name;
    }

    @Override
    public CustomerInfoAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.customer_info_layout, parent, false);

        return new CustomerInfoAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomerInfoAdapter.MyViewHolder holder, int position) {
        Item movie = moviesList.get(position);

        holder.txt_label1.setText(movie.getTrans_no()+"");
        holder.txt_label2.setText(movie.getTrans_type()+"");
        holder.txt_label3.setText(movie.getTrans_amount()+"");
        holder.txt_label4.setText(movie.getTrans_datetime()+"");
        holder.txt_label5.setText(movie.getTrans_update_balance()+"");
        holder.txt_label6.setText(movie.getStatus()+"");
        holder.txt_label7.setText(movie.getP_name()+"");
        holder.txt_label8.setText(movie.getAcno()+"");
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}

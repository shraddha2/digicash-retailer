package com.digicash.retailer;

import android.app.Application;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

public class DijiCashRetailer extends Application {

private static DijiCashRetailer mInstance;
        private FirebaseAnalytics mFirebaseAnalytics;
@Override
public void onCreate() {
        super.onCreate();

        mInstance = this;
        // Obtain the FirebaseAnalytics instance.
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        }

public static synchronized DijiCashRetailer getInstance() {
        return mInstance;

        }

public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
        }


}
package com.digicash.retailer.SendMoney;

import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.LastTenTransaction.CustomSpinnerAdapter;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.regex.Pattern;

public class AddCustomerActivity extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    EditText edt_fname,edt_lname,edt_mobile_no;
    CustomSpinnerAdapter sp_adapter = null;
    Button btn_submit;
    String fname,lname,mobile_no;
    SharedPreferences sharedpreferences;
    SharedPreferences.Editor editor;
    String token,customer_id,new_customer_otp;
    String CONFIRM_URL="", senderName="";
    TextView txt_error;
    ConstraintLayout progress_linear,linear_container;
    private String otp_up="";
    String errorMsg;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.activity_add_customer, container, false);

        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);

        /*String mess = getResources().getString(R.string.app_name);
        setTitle(mess);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);*/

  /*      mobile_no=getIntent().getStringExtra("mobile_number");
        senderName=getIntent().getStringExtra("senderName");*/

        Bundle bundle = getArguments();
        mobile_no = bundle.getString("mobile_number");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijiCashRetailer.getInstance().MODE_PRIVATE);
        editor = sharedpreferences.edit();
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        edt_fname=(EditText) rootView.findViewById(R.id.edt_fname);
        edt_lname=(EditText) rootView.findViewById(R.id.edt_lname);
        edt_mobile_no=(EditText) rootView.findViewById(R.id.edt_mobile_number);
        txt_error=(TextView)rootView.findViewById(R.id.txt_error);
        btn_submit=(Button) rootView.findViewById(R.id.btn_submit);

        edt_mobile_no.setText(mobile_no+"");

        progress_linear= (ConstraintLayout) rootView.findViewById(R.id.loading);
        linear_container= (ConstraintLayout) rootView.findViewById(R.id.container);
        progress_linear.setVisibility(View.GONE);
        linear_container.setVisibility(View.VISIBLE);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(getActivity());
                if (checkConnection()) {
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    fname=edt_fname.getText().toString();
                    lname=edt_lname.getText().toString();
                    mobile_no=edt_mobile_no.getText().toString();

                    boolean isError=false;
                    if(null==fname||fname.length()==0||fname.equalsIgnoreCase("")|| !isValidName(fname))
                    {
                        isError=true;
                        edt_fname.setError("Field Cannot be Blank");
                    }
                    if(null==lname||lname.length()==0||lname.equalsIgnoreCase("")|| !isValidName(lname))
                    {
                        isError=true;
                        edt_lname.setError("Field Cannot be Blank");
                    }
                    if(null==mobile_no||mobile_no.length()==0||mobile_no.equalsIgnoreCase("")|| mobile_no.length()<10)
                    {
                        isError=true;
                        edt_mobile_no.setError("Invalid mobile number");
                    }

                    if (!isError) {
                        new AddCustomer().execute();
                    }
                } else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        return  rootView;
    }

    public final static boolean isValidName(String target) {
        return Pattern.compile("^[A-Za-z]+$").matcher(target).matches();
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }*/

    private class AddCustomer extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("firstName", fname);
                parameters.put("lastName", lname);
                parameters.put("mobileNo", mobile_no);

                this.response = new JSONObject(service.POST(DefineData.ADD_CUSTOMER,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("AddCustomer", response + " "+fname+" "+lname+" " + mobile_no + "" );
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    } if (responseCode==20)
                    {
                        if(response.length()!=0) {
                            linear_container.setVisibility(View.VISIBLE);
                            progress_linear.setVisibility(View.GONE);
                            JSONObject json=response.getJSONObject("data");
                            customer_id = json.getString("sender_id");
                            Log.d("customerid",customer_id);
                            showChangeLangDialog(false,"");
                        }
                    }
                    else if (responseCode==23){
                        Toast.makeText(getActivity(),msg+"", Toast.LENGTH_LONG).show();
                        Fragment frg = new CustomerListFragment();
                        editor.putString(DefineData.CUSTOMER_MOBILE_NO, mobile_no);
                        Log.d("cfts",mobile_no+"");
                        editor.commit();
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg, "Add Customer")
                                .addToBackStack(null)
                                .commit();
                        progress_linear.setVisibility(View.GONE);
                    }
                    else{
                        txt_error.setText(msg);
                        txt_error.setVisibility(View.VISIBLE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Something went wrong");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Something went wrong");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    private void showChangeLangDialog(boolean isError, String msg) {
        final View dialogView = View.inflate(getActivity(),R.layout.custom_otp_dialog,null);

        final Dialog dialog = new Dialog(getActivity(),R.style.MyAlertDialogStyle);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        final EditText edt_otp=(EditText)dialog.findViewById(R.id.input_otp);
        final TextView txt_Error=(TextView) dialog.findViewById(R.id.serverError);
        Button btn_verify = (Button) dialog.findViewById(R.id.btn_verify);
        Button btn_resend = (Button) dialog.findViewById(R.id.btn_resend);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);

        if(isError)
        {
            //txt_Error.setError(errorMsg+"");
            txt_Error.setText(errorMsg+"");
        }

        btn_verify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp_up=edt_otp.getText().toString();
                //dialog.dismiss();
                //Log.d("dcsgfgh",sender_id+" "+otp_up);
                //new Verifyotp().execute();
                if (checkConnection()) {
                    boolean isError=false;

                    if(null==otp_up||otp_up.length()==0||otp_up.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_otp.setError("Field Cannot be Blank");
                    }
                    if(!isError)
                    {
                        new Verifyotp().execute();
                        dialog.dismiss();
                    }

                }else
                {
                   /*txt_Error.setVisibility(View.VISIBLE);
                    txt_Error.setText("Internet Not Available");*/
                }
            }
        });

        btn_resend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new VerifyCustomer().execute();
            }
        });

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //onBackPressed();
            }
        });

        dialog.show();
    }

    private class Verifyotp extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<>();
                parameters.put("sender_id",customer_id);
                parameters.put("otp",otp_up);
                this.response = new JSONObject(service.POST(DefineData.VERIFY_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("verifyOtp",response+"" + customer_id+ " "+ otp_up+" ");
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                        errorMsg = msg;
                    }
                    if (responseCode==1)
                    {
                       String message = response.getString("responseDesc");
                        JSONObject json=response.getJSONObject("data");
                        mobile_no = json.getString("senderMobileNo");
                        //edt_search_num.setText(mobile_number+"");
                        Log.d("regfye",message+"");
                        Toast.makeText(getActivity(),message+"", Toast.LENGTH_LONG).show();
                        Fragment frg = new CustomerListFragment();
                        editor.putString(DefineData.CUSTOMER_MOBILE_NO, mobile_no);
                        Log.d("jfbjhr",mobile_no+" ");
                        editor.commit();
                        getActivity().getFragmentManager().beginTransaction()
                                .replace(R.id.frg_replace, frg, "Add Customer")
                                .addToBackStack(null)
                                .commit();
                        progress_linear.setVisibility(View.GONE);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                    else{
                        showChangeLangDialog(true,msg);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class VerifyCustomer extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("mobileNo",customer_id);
                this.response = new JSONObject(service.POST(DefineData.RESEND_OTP,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    boolean error=response.getBoolean("error");
                    Integer responseCode=response.getInt("responseCode");
                    String msg="";
                    if(error) {
                        msg = response.getString("responseDesc");
                    }
                    if (responseCode==1)
                    {
                        linear_container.setVisibility(View.GONE);
                        progress_linear.setVisibility(View.GONE);

                        showChangeLangDialog(false,"");
                    }
                    else{

                        //edt_search_num.setText("Error Code : "+responseCode);
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }
    @Override
    public void onResume() {
        super.onResume();
        // register connection status listener
        DijiCashRetailer.getInstance().setConnectivityListener(this);
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputManager = (InputMethodManager) activity
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View currentFocusedView = activity.getCurrentFocus();
        if (currentFocusedView != null) {
            inputManager.hideSoftInputFromWindow(currentFocusedView.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

}

package com.digicash.retailer.Recharge;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;
import com.digicash.retailer.Recharge.BrowseRechargePlan.BrowsePlanActivity;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class PrepaidRechargeFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {
    private final int REQUEST_CODE=99;
    EditText edt_mobile_number;
    public  static final int RequestPermissionCode  = 1 ;
    //Typeface font ;
    TextView txt_title,txt_label1,txt_label2,txt_label3,txt_label4,txt_label5,txt_label6,txt_label7;
    TextView edt_operator_circle;
    EditText edt_amt;
    String operator_name,operator_id;
    String amt,mobile_number,fetch_amt;
    String token="",operator_circle_id="8",operator_circle_name="Maharashtra/Goa";
    SharedPreferences sharedpreferences;
    Button btn_recharge,viewPlan,specialPlan,checkValidity;
    TextView txt_error;
    LinearLayout progress_linear,linear_container;
    ImageView img_logo;
    SharedPreferences.Editor editor;
    int drawable_icon;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;

    public PrepaidRechargeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_prepaid_recharge, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");
        btn_recharge= (Button) rootView.findViewById(R.id.btn_recharge);
        viewPlan= (Button) rootView.findViewById(R.id.viewPlan);
        specialPlan= (Button) rootView.findViewById(R.id.specialPlan);
        checkValidity= (Button) rootView.findViewById(R.id.checkValidity);

        txt_error= (TextView) rootView.findViewById(R.id.txt_error);
        txt_title= (TextView) rootView.findViewById(R.id.txt_title);
        txt_label1= (TextView) rootView.findViewById(R.id.txt_label1);
        txt_label2= (TextView) rootView.findViewById(R.id.txt_label2);
        txt_label3= (TextView) rootView.findViewById(R.id.txt_label3);
        txt_label4= (TextView) rootView.findViewById(R.id.txt_label4);
        txt_label5= (TextView) rootView.findViewById(R.id.txt_label5);
        txt_label6= (TextView) rootView.findViewById(R.id.txt_label6);
        txt_label7= (TextView) rootView.findViewById(R.id.txt_label7);
        edt_operator_circle= (TextView) rootView.findViewById(R.id.edt_operator_circle);
        edt_mobile_number= (EditText) rootView.findViewById(R.id.edt_mobile_number);
        edt_amt= (EditText) rootView.findViewById(R.id.edt_amt);
        img_logo= (ImageView) rootView.findViewById(R.id.img_logo);
        progress_linear= (LinearLayout) rootView.findViewById(R.id.loding);
        linear_container= (LinearLayout) rootView.findViewById(R.id.container);

        operator_circle_name=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE,"Maharashtra/Goa");
        operator_circle_id=sharedpreferences.getString(DefineData.KEY_OPERATOR_CIRCLE_ID,"8");

        progress_linear.setVisibility(View.GONE);
        container.setVisibility(View.VISIBLE);

        txt_title.setText("Prepaid Recharge");

        viewPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
        /*        Bundle bundle = new Bundle();
                bundle.putString("operator_id", operator_id + "");
                Fragment frg = new ViewPlanFragment();
                frg.setArguments(bundle);
                getActivity().getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg, "Beneficiary Lists")
                        .addToBackStack(null)
                        .commit();*/
               /* Intent i=new Intent(getActivity(), ViewPlanFragment.class);
                i.putExtra("operator_id",operator_id);
                startActivityForResult(i, 21);*/

                Intent intent3=new Intent(getActivity(), BrowsePlanActivity.class);
                intent3.putExtra("operator_id",operator_id);
                startActivityForResult(intent3, 21);
            }
        });

        specialPlan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile_number= edt_mobile_number.getText().toString();
                if (checkConnection()) {
                    boolean isError = false;
                    if (null == mobile_number || mobile_number.length() < 10 || mobile_number.equalsIgnoreCase("")) {
                        isError = true;
                        edt_mobile_number.setError("Invalid Mobile Number");
                    }
                    if (!isError) {
                        Intent i=new Intent(getActivity(), SpecialPlanActivity.class);
                        i.putExtra("operator_id",operator_id);
                        i.putExtra("mobile_number",mobile_number);
                        startActivityForResult(i, 22);
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        checkValidity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mobile_number= edt_mobile_number.getText().toString();
                if (checkConnection()) {
                    boolean isError = false;
                    if (null == mobile_number || mobile_number.length() < 10 || mobile_number.equalsIgnoreCase("")) {
                        isError = true;
                        edt_mobile_number.setError("Invalid Mobile Number");
                    }
                    if (!isError) {
                        Intent i=new Intent(getActivity(), CheckValidityActivity.class);
                        i.putExtra("operator_id",operator_id);
                        i.putExtra("mobile_number",mobile_number);
                        i.putExtra("operator_name",operator_name);
                        startActivityForResult(i, 22);
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        btn_recharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                amt= edt_amt.getText().toString();
                mobile_number= edt_mobile_number.getText().toString();
                if (checkConnection()) {
                    boolean isError=false;
                    if(null==amt||amt.length()==0||amt.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_amt.setError("Field Cannot be Blank");
                    }
                    if(null==mobile_number||mobile_number.length()<10||mobile_number.equalsIgnoreCase(""))
                    {
                        isError=true;
                        edt_mobile_number.setError("Invalid Mobile Number");
                    }

                    if(!isError)
                    {
                        /*if(operator_id.equalsIgnoreCase("5")||operator_id.equalsIgnoreCase("6")) {
                            int amtt= Integer.parseInt(amt);
                            if ((amtt % 10) == 0) {
                                // number is even
                                operator_id="5";
                                operator_name="Bsnl-Topup";
                            } else {
                                operator_id="6";
                                operator_name="Bsnl-STV";
                        }
                            ChangeIcon(operator_id);
                        }*/
                        editor.putBoolean(DefineData.KEY_IS_CIRCLE_SAVE, true);
                        editor.putString(DefineData.KEY_OPERATOR_CIRCLE, operator_circle_name);
                        editor.putString(DefineData.KEY_OPERATOR_CIRCLE_ID, operator_circle_id);
                        editor.commit();
                        custdialog();
                    }
                }else {
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setTextColor(ContextCompat.getColor(getActivity(),R.color.status_fail));
                    txt_error.setText("No Internet Connection");
                }
            }
        });

        Bundle bundle = getArguments();
        operator_name = bundle.getString("operator_name");
        operator_id = bundle.getString("operator_id");
        txt_label2.setText(operator_name + "");
        edt_operator_circle.setText(operator_circle_name + "");
        ChangeIcon(operator_id);

        edt_mobile_number.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                final int DRAWABLE_LEFT = 0;
                final int DRAWABLE_TOP = 1;
                final int DRAWABLE_RIGHT = 2;
                final int DRAWABLE_BOTTOM = 3;

                if(event.getAction() == MotionEvent.ACTION_UP) {
                    if(event.getRawX() >= (edt_mobile_number.getRight() - edt_mobile_number.getCompoundDrawables()[DRAWABLE_RIGHT].getBounds().width())) {
                        EnableRuntimePermission();

                        // Here, thisActivity is the current activity
                        if (ContextCompat.checkSelfPermission(getActivity(),
                                Manifest.permission.READ_CONTACTS)
                                != PackageManager.PERMISSION_GRANTED) {

                            // Should we show an explanation?
                            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                    Manifest.permission.READ_CONTACTS)) {

                                // Show an explanation to the user *asynchronously* -- don't block
                                // this thread waiting for the user's response! After the user
                                // sees the explanation, try again to request the permission.

                            } else {

                                // No explanation needed, we can request the permission.

                                ActivityCompat.requestPermissions(getActivity(),
                                        new String[]{Manifest.permission.READ_CONTACTS},
                                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

                                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                // app-defined int constant. The callback method gets the
                                // result of the request.
                            }
                        }else{
                            Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                            startActivityForResult(intent, REQUEST_CODE);
                        }
                    }
                }
                return false;
            }
        });

        txt_label3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),OperatorCircleActivity.class);
                intent.putExtra("source","rech");
                startActivityForResult(intent, 3);

            }
        });

        return rootView;
    }

    public void EnableRuntimePermission(){

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.READ_CONTACTS))
        {
            ActivityCompat.requestPermissions(getActivity(),new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);
            Toast.makeText(getActivity(),"CONTACTS permission allows us to Access CONTACTS app", Toast.LENGTH_LONG).show();

        } else {

            ActivityCompat.requestPermissions(getActivity(),new String[]{
                    Manifest.permission.READ_CONTACTS}, RequestPermissionCode);

        }
    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case (REQUEST_CODE):
                if (resultCode == Activity.RESULT_OK) {
                    Uri contactData = data.getData();
                    Cursor c = getActivity().getContentResolver().query(contactData, null, null, null, null);
                    if (c.moveToFirst()) {
                        String contactId = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                        String hasNumber = c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
                        String num = "";
                        if (Integer.valueOf(hasNumber) == 1) {
                            Cursor numbers = getActivity().getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + contactId, null, null);
                            while (numbers.moveToNext()) {
                                num = numbers.getString(numbers.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                                edt_mobile_number.setText(phoeNumberWithOutCountryCode(num)+ "");
                                // Toast.makeText(getActivity(), "Number="+num, Toast.LENGTH_LONG).show();
                            }
                        }
                    }
                }
                break;
            case 2:
                if (null!=data) {
                    operator_id = data.getStringExtra("operator_id");
                    operator_name = data.getStringExtra("operator_name");
                    txt_label2.setText(operator_name + "");
                    ChangeIcon(operator_id);
                }
                break;
            case 3:
                if (null!=data) {
                    operator_circle_id = data.getStringExtra("operator_circle_id");
                    operator_circle_name = data.getStringExtra("operator_circle_name");
                    edt_operator_circle.setText(operator_circle_name + "");
                }else{
                    Bundle bundle=new Bundle();
                    bundle.putString("operator_name", operator_name);
                    bundle.putString("operator_id", operator_id);
                    android.app.Fragment frg = new PrepaidRechargeFragment();
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Prepaid")
                            .addToBackStack(null)
                            .commit();
                }
                break;
            case 5:
                break;
            case 21:
                if (null!=data) {
                    fetch_amt = data.getStringExtra("amount");
                    //Log.d("ryet", bank_id + " ");
                    edt_amt.setText(fetch_amt + "");
                }
                break;
            case 22:
                if (null!=data) {
                    fetch_amt = data.getStringExtra("operator_circle_name");
                    //Log.d("ryet", bank_id + " ");
                    edt_amt.setText(fetch_amt + "");
                }
                break;
        }}

    private class RechargePrepaid extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            linear_container.setVisibility(View.GONE);
            progress_linear.setVisibility(View.VISIBLE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("recharge_no", mobile_number);
                //parameters.put("recharge_circle", operator_circle_id);
                parameters.put("recharge_operator", operator_id);
                parameters.put("recharge_amount", amt);
                parameters.put("request_origin", "app");
                this.response = new JSONObject(service.POST(DefineData.RECHARGE_URL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String message = response.getString("data");
                        txt_error.setVisibility(View.VISIBLE);
                        txt_error.setText(message+"");
                        linear_container.setVisibility(View.VISIBLE);
                        progress_linear.setVisibility(View.GONE);
                    } else {

                        String msg=response.getString("data");
                        Intent i=new Intent(getActivity(), SuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","Prepaid Recharge");
                        startActivity(i);
                        getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txt_error.setVisibility(View.VISIBLE);
                    txt_error.setText("Error in parsing response");
                    linear_container.setVisibility(View.VISIBLE);
                    progress_linear.setVisibility(View.GONE);
                }
            }else{
                txt_error.setVisibility(View.VISIBLE);
                txt_error.setText("Empty Server Response");
                linear_container.setVisibility(View.VISIBLE);
                progress_linear.setVisibility(View.GONE);
            }
        }
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    private void ChangeIcon(String id)
    {
        switch(id)
        {
            case "1":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "2":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(getActivity()).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "3":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(getActivity()).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "4":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(getActivity()).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "5":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(getActivity()).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "6":
                drawable_icon=R.drawable.ic_bsnl;
                Picasso.with(getActivity()).load(R.drawable.ic_bsnl).fit().into(img_logo);
                break;
            case "7":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(getActivity()).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "8":
                drawable_icon=R.drawable.ic_telenor;
                Picasso.with(getActivity()).load(R.drawable.ic_telenor).fit().into(img_logo);
                break;
            case "9":
                drawable_icon=R.drawable.ic_jio;
                Picasso.with(getActivity()).load(R.drawable.ic_jio).fit().into(img_logo);
                break;
            case "10":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "11":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "12":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(getActivity()).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "13":
                drawable_icon=R.drawable.ic_mtnl;
                Picasso.with(getActivity()).load(R.drawable.ic_mtnl).fit().into(img_logo);
                break;
            case "14":
                drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
                break;
            case "15":
                drawable_icon=R.drawable.ic_idea_logo;
                Picasso.with(getActivity()).load(R.drawable.ic_idea_logo).fit().into(img_logo);
                break;
            case "16":
                drawable_icon=R.drawable.ic_vodafone;
                Picasso.with(getActivity()).load(R.drawable.ic_vodafone).fit().into(img_logo);
                break;
            case "17":
                drawable_icon=R.drawable.ic_aircel;
                Picasso.with(getActivity()).load(R.drawable.ic_aircel).fit().into(img_logo);
                break;
            case "18":
                drawable_icon=R.drawable.ic_docomo;
                Picasso.with(getActivity()).load(R.drawable.ic_docomo).fit().into(img_logo);
                break;
            case "19":
                drawable_icon=R.drawable.ic_airtel_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel_tv).fit().into(img_logo);
                break;
            case "20":
                drawable_icon=R.drawable.ic_tata_sky;
                Picasso.with(getActivity()).load(R.drawable.ic_tata_sky).fit().into(img_logo);
                break;
            case "21":
                drawable_icon=R.drawable.ic_dish_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_dish_tv).fit().into(img_logo);
                break;
            case "22":
                drawable_icon=R.drawable.ic_videocon;
                Picasso.with(getActivity()).load(R.drawable.ic_videocon).fit().into(img_logo);
                break;
            case "23":
                drawable_icon=R.drawable.ic_big_tv;
                Picasso.with(getActivity()).load(R.drawable.ic_big_tv).fit().into(img_logo);
                break;
            case "24":
                drawable_icon=R.drawable.ic_sun_direct;
                Picasso.with(getActivity()).load(R.drawable.ic_sun_direct).fit().into(img_logo);
                break;
            case "38":
                drawable_icon=R.drawable.ic_zing;
                Picasso.with(getActivity()).load(R.drawable.ic_zing).fit().into(img_logo);
                break;
            default:   drawable_icon=R.drawable.ic_airtel;
                Picasso.with(getActivity()).load(R.drawable.ic_airtel).fit().into(img_logo);
        }
    }

    public String phoeNumberWithOutCountryCode(String num) {
        if (num.startsWith("+91")) {
            num = num.replaceFirst("\\+(91)", "");}
        return num;
    }

    @Override
    public void onResume() {

        super.onResume();
        DijiCashRetailer.getInstance().setConnectivityListener(this);
        getActivity().getCurrentFocus().clearFocus();
        txt_label3.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    txt_label3.setFocusableInTouchMode(false);
                    txt_label3.clearFocus();
                    getView().requestFocus();
                    getView().setFocusableInTouchMode(true);
                }
                return false;
            }
        });
        edt_mobile_number.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_mobile_number.setFocusableInTouchMode(false);
                    edt_mobile_number.clearFocus();
                    getView().requestFocus();
                    getView().setFocusableInTouchMode(true);
                }
                return false;
            }
        });
        edt_amt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    edt_amt.setFocusableInTouchMode(false);
                    edt_amt.clearFocus();
                    getView().requestFocus();
                    getView().setFocusableInTouchMode(true);
                }
                return false;
            }
        });
        img_logo.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (keyCode == KeyEvent.KEYCODE_BACK) {
                    img_logo.setFocusableInTouchMode(false);
                    img_logo.clearFocus();
                    getView().requestFocus();
                    getView().setFocusableInTouchMode(true);
                    //return true;
                }
                return false;
            }
        });
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    ((HomeActivity) getActivity()).hideKeyboard(getActivity());
                    Fragment frg=new MobileOperatorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("trans_type", "Mobile Prepaid");
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Mobile Prepaid")
                            .addToBackStack(null)
                            .commit();
                    return true;

                }
                return false;
            }
        });
    }

    private void custdialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_dialog,null);
        final Dialog dialog = new Dialog(getActivity(),R.style.FullScreenDialogStyle);
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);
        ImageView img_logos=(ImageView)dialog.findViewById(R.id.img_logos);
        TextView txt_amt=(TextView)dialog.findViewById(R.id.txt_amt);
        TextView txt_op_name=(TextView)dialog.findViewById(R.id.txt_op_name);
        TextView txt_operator_circle=(TextView)dialog.findViewById(R.id.txt_operator_circle);
        TextView txt_rec_number=(TextView)dialog.findViewById(R.id.txt_rec_number);
        Button btn_confirm = (Button) dialog.findViewById(R.id.btn_confirm);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        Picasso.with(DijiCashRetailer.getInstance()).load(drawable_icon).fit().into(img_logos);

        txt_op_name.setText(operator_name);
        txt_amt.setText("₹ "+amt);
        txt_operator_circle.setText(operator_circle_name);
        txt_rec_number.setText(mobile_number);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        btn_confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new RechargePrepaid().execute();
            }
        });
        dialog.show();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                    startActivityForResult(intent, REQUEST_CODE);

                } else {
                    Toast.makeText(getActivity(),"Please enable permission to access contact ", Toast.LENGTH_LONG).show();
                }
                return;
            }
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

}

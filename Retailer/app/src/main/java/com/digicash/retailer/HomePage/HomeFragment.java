package com.digicash.retailer.HomePage;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.TextView;

import com.digicash.retailer.BillPayment.BillPaymentFragment;
import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.digicash.retailer.AccountReport.PaymentReportActivity;
import com.digicash.retailer.CommissionReport.CommissionReportFragment;
import com.digicash.retailer.DefineData;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Recharge.DTHOperatorFragment;
import com.digicash.retailer.Recharge.MobileOperatorFragment;
import com.digicash.retailer.SendMoney.CustomerListFragment;
import com.digicash.retailer.TransactionHistory.TransactionHistoryActivity;
import com.digicash.retailer.WalletHistory.WalletHistoryFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class HomeFragment extends Fragment {

    GridView gridView;
    private List<Item> data;
    Context ctx;
    SharedPreferences sharedpreferences;
    String token;
    TextView txt_title, txt_wallet_balance ;
    String current_balance, rechargePercent, mtPercent;
    SharedPreferences.Editor editor;
    TextView btn_more_details,txtNotification;
    private PieChart mChart;
    Double retailerPercentage=0.0, mtPercentage=0.0, bbpsPercentage=0.0;
    String mobile_number="",notification="";

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView=inflater.inflate(R.layout.fragment_home, container, false);

        getActivity().setTitle("DijiCash");

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        current_balance=sharedpreferences.getString(DefineData.KEY_WALLET_BALANCE,"");
        rechargePercent=sharedpreferences.getString(DefineData.KEY_PERCENTAGE,"");
        mtPercent=sharedpreferences.getString(DefineData.KEY_PERCENTAGE,"");
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txtNotification = (TextView) rootView.findViewById(R.id.txtNotification);
        txtNotification.setSelected(true);

        btn_more_details = (TextView) rootView.findViewById(R.id.btn_more_details);

        gridView = (GridView) rootView.findViewById(R.id.gridview);
        txt_title=(TextView)rootView.findViewById(R.id.txt_title);
        gridView = (GridView) rootView.findViewById(R.id.gridview);

        new FetchBalance().execute();
        new FetchNotification().execute();

        data = fill_with_data();
        ctx=getActivity();
        GridRechargeAdapter booksAdapter = new GridRechargeAdapter(data, getActivity());
        gridView.setAdapter(booksAdapter);

        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String list = data.get(position).txt.toString();

                if(list.equalsIgnoreCase("Mobile Prepaid"))
                {
                    Fragment frg=new MobileOperatorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("trans_type", list);
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Mobile Prepaid")
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("DTH"))
                {
                    Fragment frg=new DTHOperatorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("trans_type", list);
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"DTH")
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Mobile Postpaid"))
                {
                    Fragment frg=new MobileOperatorFragment();
                    Bundle bundle=new Bundle();
                    bundle.putString("trans_type", list);
                    frg.setArguments(bundle);
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Mobile Postpaid")
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Send Money"))
                {
                    Fragment frg=new CustomerListFragment();
                    editor.putString(DefineData.CUSTOMER_MOBILE_NO, mobile_number);
                    editor.commit();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Wallet History"))
                {
                    Fragment frg=new WalletHistoryFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Transaction History"))
                {
                    Intent i=new Intent(getActivity().getApplicationContext(), TransactionHistoryActivity.class);
                    startActivity(i);
                }
                if(list.equalsIgnoreCase("Commission Report"))
                {
                    Fragment frg=new CommissionReportFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                if(list.equalsIgnoreCase("Payment Report"))
                {
                    Intent i=new Intent(getActivity().getApplicationContext(), PaymentReportActivity.class);
                    startActivity(i);
                }
                if(list.equalsIgnoreCase("Bill Payment"))
                {
                    Fragment frg=new BillPaymentFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }
                /*  if(list.equalsIgnoreCase("DTH Booking"))
                {
                    Intent i=new Intent(getActivity().getApplicationContext(), DthBookingActivity.class);
                    startActivity(i);
                }
                if(list.equalsIgnoreCase("DTH Booking History"))
                {
                    Fragment frg=new DthBookingHistoryFragment();
                    ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg)
                            .addToBackStack(null)
                            .commit();
                }*/
            }
        });

        /*-- Pie Chart Code --*/
        mChart = (PieChart) rootView.findViewById(R.id.chart1);
        //mChart.setBackgroundColor(Color.WHITE);

        moveOffScreen();

        mChart.setUsePercentValues(true);
        mChart.getDescription().setEnabled(false);
        mChart.setDrawHoleEnabled(false);

        //To the make the chart half
        mChart.setMaxAngle(180);
        mChart.setRotationAngle(180);
        mChart.setCenterTextOffset(0, -20);
        mChart.setRotationEnabled(false); //this will stop rotating the pie chart

        //Animation
        mChart.animateY(1000, Easing.EasingOption.EaseInBack.EaseInOutCubic);  //animate with 1000 ms

        mChart.setEntryLabelColor(Color.WHITE);
        mChart.setEntryLabelTextSize(12f);

        btn_more_details.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frg=new MoreDetailsFragment();
                ((FragmentActivity) ctx).getFragmentManager().beginTransaction()
                        .replace(R.id.frg_replace, frg)
                        .addToBackStack(null)
                        .commit();
            }
        });
        return rootView;
    }

    public List<Item> fill_with_data() {
        List<Item> data = new ArrayList<>();

        data.add(new Item( R.drawable.ic_prepaid, "Mobile Prepaid","1"));
        data.add(new Item( R.drawable.ic_dth_payment, "DTH","2"));
        data.add(new Item( R.drawable.ic_postpaid, "Mobile Postpaid","3"));
        //data.add(new Item( R.drawable.ic_bill_payment, "Bill Payment","4"));
        data.add(new Item( R.drawable.ic_send_money, "Send Money","4"));
        data.add(new Item( R.drawable.ic_wallet_history, "Wallet History","5"));
        data.add(new Item( R.drawable.ic_transaction_history, "Transaction History","6"));
        data.add(new Item( R.drawable.ic_commission_report, "Commission Report","7"));
        data.add(new Item( R.drawable.ic_payment_report, "Payment Report","8"));
        data.add(new Item( R.drawable.ic_bill_payment, "Bill Payment","9"));

       /*  data.add(new Item( R.drawable.ic_dth_booking, "DTH Booking","10"));
        data.add(new Item( R.drawable.ic_dth_booking_history, "DTH Booking History","11"));*/
        return data;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((HomeActivity) getActivity()).hideKeyboard(getActivity());
        ((HomeActivity) getActivity()).selectNavigationDrawerItem(0);
    }

    private class FetchBalance extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                this.response = new JSONObject(service.POST(DefineData.FETCH_BALANCE,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                String balance="", announcement="";
                try {
                    if (response.getBoolean("error")) {
                        balance="...";
                        retailerPercentage=0.0;
                        mtPercentage=0.0;
                        bbpsPercentage=0.0;
                    } else {
                        //announcement= response.getString("announcement")+"";
                        balance= String.format( "%.2f", response.getDouble("balance"))+"";
                        retailerPercentage= response.getDouble("rechargePercentage");
                        mtPercentage= response.getDouble("mtPercentage");
                        bbpsPercentage= response.getDouble("bbpsPercentage");
                        //Log.d("rechargemt", retailerPercentage+" "+ mtPercentage+" "+bbpsPercentage);
                        setData(3, 100); //this will set data to 4 parts of 100%
                    }
                    current_balance=balance;
                    //txt_wallet_balance.setText(" ₹ "+balance+"");
                    ((HomeActivity) getActivity()).updateWalletBalance(balance);
                    editor.putString(DefineData.KEY_WALLET_BALANCE, balance);
                    editor.commit();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private class FetchNotification extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {

        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("userType", "rr");
                this.response = new JSONObject(service.POST(DefineData.FETCH_NOTIFICATION,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                String announcement="";
                try {
                    if (response.getBoolean("error")) {

                    } else {
                        announcement= response.getString("data")+"";
                    }
                    notification=announcement;
                    txtNotification.setText(notification+"");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /*-- Pie Chart Code --*/

    private void setData(int count, int range){
        ArrayList<PieEntry> values = new ArrayList<>();
        String[] countries = new String[]{"Recharge","MT","BBPS"};  //Array of string

        values.add(new PieEntry(Float.valueOf(String.valueOf(retailerPercentage)), countries[0]));
        values.add(new PieEntry(Float.valueOf(String.valueOf(mtPercentage)), countries[1]));
        values.add(new PieEntry(Float.valueOf(String.valueOf(bbpsPercentage)), countries[2]));
        PieDataSet dataSet = new PieDataSet(values, "Sales");  //partner is the label of the pie chart
        dataSet.setSelectionShift(5f);  //gape between the chart chart nd on click of that
        dataSet.setSliceSpace(3f);
        dataSet.setColors(ColorTemplate.COLORFUL_COLORS);  //color of the set

        //pie data
        PieData data = new PieData(dataSet);
        data.setValueFormatter(new PercentFormatter()); //Will Display value in percentage
        data.setValueTextSize(15f);
        data.setValueTextColor(Color.WHITE);

        mChart.setData(data);
        mChart.setNoDataText("No Data Available");
        //mChart.setNoDataTextColor(Color.CYAN);

        mChart.invalidate();
    }

    //moving the pie chart (i.e. top, bottom nd so on)
    private void moveOffScreen(){
        Display display = getActivity().getWindowManager().getDefaultDisplay(); //Display Size
        DisplayMetrics metrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(metrics);  //pass the matrix in the window
        int height = metrics.heightPixels;    //Display height of the matrix

        int offset = (int) (height*0.5); //this chart will be somewhere middle of the display(i.e. half(0.5))
        mChart.setExtraOffsets(0,10,0,0);
        ConstraintLayout.LayoutParams params = (ConstraintLayout.LayoutParams)mChart.getLayoutParams();//Relative layout parameter(params)
        params.setMargins(0, 0, 0, -offset); //this will move the pie from the middle to the end
        mChart.setLayoutParams(params);
    }
}
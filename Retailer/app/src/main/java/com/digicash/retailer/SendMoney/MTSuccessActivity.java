package com.digicash.retailer.SendMoney;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.Model.DividerItemDecoration;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.Model.Item;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.OfflineMode.OfflineModeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Recharge.SuccessActivity;
import com.digicash.retailer.TransactionHistory.RechargeAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MTSuccessActivity extends AppCompatActivity {

    private List<Item> TransactionList = new ArrayList<>();
    TextView txt_msg;
    String type;
    SharedPreferences sharedpreferences;
    String token;
    private RecyclerView recyclerView;
    private MtSuccessAdapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mtsuccess_activity);

        sharedpreferences = getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, Context.MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        txt_msg=(TextView)  findViewById(R.id.txt_msg);
        String msg=getIntent().getStringExtra("msg");
        txt_msg.setText(msg);

        type=getIntent().getStringExtra("type");

        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mAdapter = new MtSuccessAdapter(TransactionList,MTSuccessActivity.this,"Mt Transaction");
        recyclerView.addItemDecoration(new DividerItemDecoration(MTSuccessActivity.this));

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(MTSuccessActivity.this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

    }

    /*private class FetchRechargesHistory extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            TransactionList.clear();
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("amount" , transfer_amount);
                parameters.put("senderId", cust_id);
                parameters.put("beneficiaryId", beneficiaryId);
                parameters.put("remitType", trans_type);
                parameters.put("request_origin", "app");
                parameters.put("totalSurcharge",token);
                parameters.put("senderPhone", sender_no);
                this.response = new JSONObject(service.POST(DefineData.SEND_MONEY,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }

                    } else {
                        JSONArray jsonArray=response.getJSONArray("transactionDetails");
                        if(jsonArray.length()!=0) {
                            for (int i = 0; i < jsonArray.length(); i++) {
                                Item superHero = null;
                                JSONObject json2 = null;
                                try {

                                    json2 = jsonArray.getJSONObject(i);

                                    String no = json2.getString("no");
                                    String amount = json2.getString("amount");
                                    String bankRefNo = json2.getString("bankRefNo");
                                    String transactionId = json2.getString("transactionId");
                                    String status = json2.getString("status");
                                    superHero = new Item(no, "₹ " + amount,transactionId,bankRefNo, status);

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                //Adding the superhero object to the list
                                TransactionList.add(superHero);
                            }

                            mAdapter.notifyDataSetChanged();
                        }else{

                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{

            }

        }
    }*/

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent i;
        if(type.equalsIgnoreCase("Offline Recharge"))
        {
            i = new Intent(MTSuccessActivity.this, OfflineModeActivity.class);
        }else {
            i = new Intent(MTSuccessActivity.this, HomeActivity.class);
        }
        startActivity(i);
        finish();
    }
}

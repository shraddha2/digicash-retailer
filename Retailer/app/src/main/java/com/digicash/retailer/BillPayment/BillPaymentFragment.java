package com.digicash.retailer.BillPayment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Fragment;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.digicash.retailer.DefineData;
import com.digicash.retailer.DijiCashRetailer;
import com.digicash.retailer.HomePage.HomeFragment;
import com.digicash.retailer.Model.HTTPURLConnection;
import com.digicash.retailer.NavigationDrawer.HomeActivity;
import com.digicash.retailer.R;
import com.digicash.retailer.Receiver.ConnectivityReceiver;
import com.digicash.retailer.Recharge.SuccessActivity;
import com.digicash.retailer.SendMoney.SendMoneyActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class BillPaymentFragment extends Fragment implements ConnectivityReceiver.ConnectivityReceiverListener {

    TextView textView25,textView26,txtError;
    EditText edtServiceProvider,edtConsumerNo,edtBillingUnit,edtMobileNo;
    Button btnSubmit;
    ConstraintLayout fetchBillerDetails,ConsumerNo,BillingUnit,loading,content;
    SharedPreferences sharedpreferences;
    String token="",getBillerid="",getBillerOperatorId="",getBillerOperatorName="",fetBillerParam="";
    String parameter1="",parameter2="",consumerNo,billingUnit,mobileNo;
    String storedConsumerId="",storedBillAmount="",storeSurcharge="",storedTotalAmount="",storeConsumerName="",storeBillDueDate="",storeBillNumber="",storedBillDate="",referenceID;

    public BillPaymentFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_bill_payment, container, false);
        String mess = getResources().getString(R.string.app_name);
        getActivity().setTitle(mess);
        setHasOptionsMenu(true);

        sharedpreferences = getActivity().getSharedPreferences(DefineData.LOGIN_MINKSPAY_PREFERENCE, DijiCashRetailer.getInstance().MODE_PRIVATE);
        token=sharedpreferences.getString(DefineData.TOKEN_KEY,"");

        edtServiceProvider= (EditText) rootView.findViewById(R.id.edtServiceProvider);
        edtServiceProvider.setInputType(InputType.TYPE_NULL);
        edtServiceProvider.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(getActivity(),GetBillersActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        loading = (ConstraintLayout) rootView.findViewById(R.id.loading);
        content = (ConstraintLayout) rootView.findViewById(R.id.container);
        fetchBillerDetails = (ConstraintLayout) rootView.findViewById(R.id.fetchBillerDetails);
        ConsumerNo = (ConstraintLayout) rootView.findViewById(R.id.ConsumerNo);
        textView25 = (TextView) rootView.findViewById(R.id.textView25);
        edtConsumerNo = (EditText) rootView.findViewById(R.id.edtConsumerNo);
        BillingUnit = (ConstraintLayout) rootView.findViewById(R.id.BillingUnit);
        textView26 = (TextView) rootView.findViewById(R.id.textView26);
        edtBillingUnit = (EditText) rootView.findViewById(R.id.edtBillingUnit);
        edtMobileNo = (EditText) rootView.findViewById(R.id.edtMobileNo);
        txtError = (TextView) rootView.findViewById(R.id.txtError);
        btnSubmit = (Button) rootView.findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                consumerNo=edtConsumerNo.getText().toString();
                billingUnit=edtBillingUnit.getText().toString();
                mobileNo=edtMobileNo.getText().toString();

                boolean isError=false;

                if(null==consumerNo||consumerNo.length()==0||consumerNo.equalsIgnoreCase(""))
                {
                    isError=true;
                    edtConsumerNo.setError("Field cannot be blank");
                }
                if(BillingUnit.getVisibility()==View.VISIBLE)
                {
                    if (!isValidBillingUnit(billingUnit)) {
                        edtBillingUnit.setError("Field cannot be blank");
                        isError=true;
                    }
                } else{
                    isError=false;
                }
                if(null==mobileNo||mobileNo.length()<10||mobileNo.equalsIgnoreCase(""))
                {
                    isError=true;
                    edtMobileNo.setError("Enter 10 digit mobile number");
                }
                if(!isError) {
                    if(checkConnection()) {
                        loading.setVisibility(View.GONE);
                        new FetchBill().execute();
                    }else{
                        loading.setVisibility(View.GONE);
                    }
                }
            }
        });

        return rootView;
    }

    private class FetchBill extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("consumerId", consumerNo);
                parameters.put("option1", billingUnit);
                parameters.put("biller", getBillerOperatorId);
                parameters.put("customerNo", mobileNo);
                parameters.put("requestOrigin", "app");
                this.response = new JSONObject(service.POST(DefineData.FETCH_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("generateBill", response +"    "+"consumerNo: "+consumerNo+ " "+"billingUnit: "+billingUnit+ " "+"getBillerOperatorId: "+getBillerOperatorId+" "+
                    "mobileNo: "+mobileNo+" "+ DefineData.FETCH_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        String msg="Error";
                        if(response.has("data")) {
                            msg = response.getString("data");
                        }
                        txtError.setText(msg+"");
                        txtError.setVisibility(View.VISIBLE);
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    } else {
                        JSONObject json = response.getJSONObject("data");
                        if(json.length()!=0) {
                            try {
                                String consumerId = json.getString("consumerId");
                                String billAmount = json.getString("billAmount");
                                String surcharge = json.getString("surcharge");
                                String totalAmount = json.getString("totalAmount");
                                String consumerName = json.getString("consumerName");
                                String billDueDate = json.getString("billDueDate");
                                String billNumber = json.getString("billNumber");
                                String billDate = json.getString("billDate");
                                String referenceId = json.getString("referenceId");

                                storedConsumerId = consumerId;
                                storedBillAmount = billAmount;
                                storeSurcharge = surcharge;
                                storedTotalAmount = totalAmount;
                                storeConsumerName = consumerName;
                                storeBillDueDate = billDueDate;
                                storeBillNumber = billNumber;
                                storedBillDate = billDate;
                                referenceID = referenceId;

                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);
                            } catch (JSONException e) {
                                e.printStackTrace();
                                txtError.setText("Something went wrong! Try again");
                                loading.setVisibility(View.GONE);
                                content.setVisibility(View.VISIBLE);
                            }
                            transactionConfirmDialog();
                        }else{
                            txtError.setText("Record not Found");
                            loading.setVisibility(View.GONE);
                            content.setVisibility(View.VISIBLE);
                        }
                    }
                }  catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try again");
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);

                }
            }else{
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    private void transactionConfirmDialog(){
        final View dialogView = View.inflate(getActivity(),R.layout.confirm_generate_bill_layout,null);

        final Dialog dialog = new Dialog(getActivity());
        if (dialog != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.WRAP_CONTENT;
            dialog.getWindow().setLayout(width, height);
        }
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dialogView);

        TextView CustomerName = dialog.findViewById(R.id.txtCustomerName);
        TextView CustomerId = dialog.findViewById(R.id.txtCustomerId);
        TextView Amount= dialog.findViewById(R.id.txtAmount);
        TextView Surcharge= dialog.findViewById(R.id.txtSurcharge);
        TextView BillNumber= dialog.findViewById(R.id.txtBillNumber);
        TextView BillDate= dialog.findViewById(R.id.txtBillDate);
        TextView BillDueDate= dialog.findViewById(R.id.txtBillDueDate);
        TextView TotalAmount= dialog.findViewById(R.id.txtTotalAmount);
        Button btnPay = dialog.findViewById(R.id.btnPay);
        Button btnCancel = dialog.findViewById(R.id.btnCancel);

        CustomerName.setText(storeConsumerName+"");
        CustomerId.setText(storedConsumerId+"");
        Amount.setText("₹ "+storedBillAmount);
        Surcharge.setText("₹ "+storeSurcharge+"");
        BillNumber.setText(storeBillNumber);
        BillDate.setText(storedBillDate+"");
        BillDueDate.setText(storeBillDueDate+"");
        TotalAmount.setText("₹ "+storedTotalAmount);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                new PayBill().execute();
            }
        });
        dialog.show();
    }

    private class PayBill extends AsyncTask<Void, Void, Void> {

        JSONObject response;
        @Override
        protected void onPreExecute() {
            loading.setVisibility(View.VISIBLE);
            content.setVisibility(View.GONE);
        }
        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("operatorId" , getBillerOperatorId);
                parameters.put("consumerId", storedConsumerId);
                parameters.put("option1", billingUnit);
                parameters.put("customerNo", mobileNo);
                parameters.put("amount",storedBillAmount);
                parameters.put("requestOrigin", "app");
                parameters.put("referenceId",referenceID);
                this.response = new JSONObject(service.POST(DefineData.PAY_BILL,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            Log.d("payBill", response + " "+"getBillerOperatorId: :"+getBillerOperatorId+" "+"storedConsumerId: "+storedConsumerId +" "
                    +"billingUnit: " + billingUnit+ " "+"mobileNo: " + mobileNo + " "+"storedBillAmount: "+storedBillAmount+"referenceId: "+referenceID+" "+DefineData.PAY_BILL);
            if(response!=null) {
                try {
                    if (response.getBoolean("error")) {
                        txtError.setVisibility(View.VISIBLE);
                        txtError.setText(response.getString("data"));

                        if(response.has("insideError")) {
                            txtError.setVisibility(View.GONE);
                            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                            builder.setMessage(response.getString("data"));
                            builder.setPositiveButton("Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog,
                                                            int which) {
                                            dialog.dismiss();
                                            Intent i=new Intent(getActivity(), HomeActivity.class);
                                            getActivity().startActivity(i);
                                            getActivity().finish();
                                        }
                                    });
                            builder.show();

                        }
                        loading.setVisibility(View.GONE);
                        content.setVisibility(View.VISIBLE);
                    }
                    else {
                        String msg=response.getString("data");
                        Intent i=new Intent(getActivity(), SuccessActivity.class);
                        i.putExtra("msg",msg);
                        i.putExtra("type","Bill Payment");
                        startActivity(i);
                        getActivity().finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    txtError.setVisibility(View.VISIBLE);
                    txtError.setText("Something went wrong! Try again later");
                    loading.setVisibility(View.GONE);
                    content.setVisibility(View.VISIBLE);
                }
            }else{
                txtError.setVisibility(View.VISIBLE);
                txtError.setText("Empty response from server");
                loading.setVisibility(View.GONE);
                content.setVisibility(View.VISIBLE);
            }
        }
    }

    private boolean isValidBillingUnit(String billingUnit) {
        if (billingUnit == null || billingUnit.length() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public void onResume() {

        super.onResume();
        DijiCashRetailer.getInstance().setConnectivityListener(this);
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){

                    android.app.Fragment frg=new HomeFragment();
                    ((FragmentActivity) getActivity()).getFragmentManager().beginTransaction()
                            .replace(R.id.frg_replace, frg,"Home")
                            .addToBackStack(null)
                            .commit();

                    return true;

                }

                return false;
            }
        });
    }

    // Method to manually check connection status
    private boolean checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        return isConnected;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

    }

    @Override
    public void onActivityResult(int reqCode, int resultCode, Intent data) {
        super.onActivityResult(reqCode, resultCode, data);
        switch (reqCode) {
            case 1:
                if (null!=data) {
                    getBillerid = data.getStringExtra("getBillerid");
                    getBillerOperatorId = data.getStringExtra("getBillerOperatorId");
                    getBillerOperatorName = data.getStringExtra("getBillerOperatorName");
                    edtServiceProvider.setText(getBillerOperatorName + "");
                    fetchBillerDetails.setVisibility(View.VISIBLE);
                    new FetchBillerFields().execute();
                    fetBillerParam="";
                    edtConsumerNo.setText("");
                    edtBillingUnit.setText("");
                }
                break;
        }
    }

    private class FetchBillerFields extends AsyncTask<Void, Void, Void> {
        JSONObject response;
        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Void doInBackground(Void... params) {
            HTTPURLConnection service = new HTTPURLConnection();
            try{
                HashMap<String, String> parameters = new HashMap<String, String>();
                parameters.put("biller_id", getBillerOperatorId);
                this.response = new JSONObject(service.POST(DefineData.FETCH_BILLER_DETAILS,parameters,token));
            }catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPostExecute(Void aVoid) {
            if(response!=null) {
                try {
                    JSONObject resjson = response.getJSONObject("data");
                    JSONArray parameterName = resjson.getJSONArray("parameter_name");
                    for(int j=0;j<parameterName.length();j++){
                        fetBillerParam += parameterName.getString(j)+"\n";
                    }

                    //String to split
                    String[] parte = fetBillerParam.split("\n");
                    parameter1 = parte[0];
                    textView25.setText(parameter1+"");
                    if (parte.length==2) {
                        parameter2 = parte[1];
                        textView26.setText(parameter2+"");
                        BillingUnit.setVisibility(View.VISIBLE);
                    }
                    else
                    {
                        parameter2 = "";
                        BillingUnit.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }else{
            }
        }
    }
}